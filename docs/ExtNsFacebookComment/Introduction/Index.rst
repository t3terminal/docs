
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_facebook_comment
=======================

.. figure:: Images/ext-facebook-comment-banner.jpg
   :alt: Extension Banner 


What does it do?
================

Facebook comments plugin is a great tool that will allow you to show your visitors Facebook comments on your website’s particular page. At the same time this plugin is very useful for improving your website traffic from Facebook.

This plugin is easy to use, you just need to create Fb App ID and use it on your website.


Helpful Links
=============

.. Note::

	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support