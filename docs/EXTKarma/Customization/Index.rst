.. include:: ../Includes.txt

.. |true-icon| image:: Images/righ-icon.png
.. |false-icon| image:: Images/crose-icon.png
.. |view-icon| image:: Images/view.png
.. |version-icon| image:: Images/version.png
.. |documentation-icon| image:: Images/documentation.png
.. |typo-icon| image:: Images/typo.png

.. _customization:

=============
Customization
=============

For all customization related details, please refer here : https://docs.t3planet.com/en/latest/ExtThemes/Customization/Index.html

Enable/Disable Google font API Link to complaints with GDPR
===========================================================

**How to Disable Google Fonts?**
Could you please perform the below steps to disable Google font at your site?

Step 1. Go to Theme Options > Click on the root page.

Step 2. Click on Style section > Fonts

Step 3. Remove Google Font API Link now save changes & clear cache once.


.. figure:: Images/GoogleFonts.jpeg
   :alt: Google Fonts
   
**How to add Custom Fonts?**
Could you please perform the below steps to insert the custom font at your site?

Step 1. Go to Theme Options > Click on the root page.

Step 2. Click on Style section > Fonts

Step 3. Insert your custom font path and font family name than save changes and clear cache.


.. figure:: Images/CustomFonts.jpeg
   :alt: Custom Fonts
 
.. attention:: Disable Style Switcher from the Layouts section.

.. figure:: Images/DisableStyleSwitcher.jpeg
   :alt: Disable Style Switcher
