
.. include:: ../Includes.txt

===================
Configure Captcha
===================

If you want to configure Captcha in a form, we offer support for both Friendly Captcha and Google reCAPTCHA. Please refer to the links below for configuration details.

- `Friendly Captcha <https://docs.t3planet.com/en/latest/ExtNsFriendlyCaptcha/Index.html>`_

- `Google reCaptcha <https://docs.typo3.org/p/evoweb/recaptcha/14.0/en-us/UsersManual/Index.html>`_


