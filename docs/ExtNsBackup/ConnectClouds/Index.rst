.. include:: ../Includes.txt

======================
Connect Clouds/Servers
======================

We highly recommend to take your backup to another clouds or server, because self-hosted backup is not the secure-way. You can easily add/edit/delete your favourite clouds and servers.

.. figure:: Images/ns-backend-typo3-clouds-servers.png
   :alt: ns-backend-typo3-clouds-servers

.. Note:: If you will not add any of your clouds/servers, then extension will only take backup at your local-server at /uploads/tx_nsbackup/ folder

Add Cloud/Server
================

**Step 1.** Go to Admin Tools > NS Backup

**Step 2.** Click on Servers/Clouds Menu, And click on "Configure New Server/Cloud"

**Step 3.** Enter the name of your Server for your reference

**Step 4.** Choose Your Server/Cloud from dropdown

**Step 5.** Based on your selection of server/cloud, You will need to carefully add your server's connection configuration eg., secret key, access etc.

.. figure:: Images/ns-backend-typo3-add-new-cloud-server.png
   :alt: ns-backend-typo3-add-new-cloud-server