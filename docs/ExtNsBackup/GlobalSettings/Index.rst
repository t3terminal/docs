.. include:: ../Includes.txt

===============
Global Settings
===============

.. figure:: Images/ns-backup-typo3-global-validation.png
   :alt: ns-backup-typo3-global-validation

Once you install this extension, Your first-step should be to configure all the settings from "Global Configuration".

**Step 1.** Go to Admin Tools > NS Backup

**Step 2.** Click on "Global Settings" menu, Fill-up all the information and click on "Save Settings" button.

.. figure:: Images/ns-backup-typo3-global-settings.png
   :alt: ns-backup-typo3-global-settings

**Clean up Quantity:** This feature allows you to control how many recent backups are stored locally on your system. 

**Example**:If you set the Clea-up Quantity to 5, the system will keep only the 5 most recent backups on your Storage folder, older backup will be deleted.

Backup store Path
=====================

.. figure:: Images/Backup_path.png
   :alt: ns-backup-typo3-global-settings

This option allows you to store backups in a private directory that cannot be accessed directly through a URL.This ensures that the backup files remain secure and are not publicly accessible.

.. figure:: Images/Download.png
   :alt: ns-backup-typo3-global-settings

.. note:: For security reasons, private path backups cannot be downloaded directly from the backend as its publicly not accessible; they must be accessed from the configured server path.**
