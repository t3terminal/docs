.. include:: ../Includes.txt

.. _installation:

==================
System Requirement
==================

.. figure:: Images/ns-backup-typo3-system-requirement.png
   :alt: ns-backup-typo3-system-requirement
   

   Please make sure to have your web-server is compatible with following installation and configuration.

   +------------------+-----------------------------------------------+
   | TYPO3 version    | v8, v9, v10                                   |
   +------------------+-----------------------------------------------+
   | PHP Version      | PHP 7.0 or later                              |
   +------------------+-----------------------------------------------+
   | PHP Extensions   | ext/curl, ext/dom, ext/json                   |
   +------------------+-----------------------------------------------+
   | POSIX Shell      | tar, bzip2 or gzip                            |
   +------------------+-----------------------------------------------+

.. Hint:: For backup and scheduler, we have installed and configured one of the most popular PHPBU solution. We recommend to see https://phpbu.de/manual/current/en/installation.html#installation.requirements

.. important:: This extension may not works well on shared server, due to PHPBU's SSH command execute ".phar file" with PHP's exec() and shell_exe().
