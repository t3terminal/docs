.. include:: ../Includes.txt

========================================================
How to get Google Client ID, Secret Key & Refresh Token?
========================================================

Before you can start using EXT:ns_googledocs, You need to get Google Client ID, Secret Key and Refresh Token for your Gmail account. These details will help to fetch Google Docs from your Gmail account.

1. To Create Google Client ID and Secret Key go to https://console.developers.google.com

2. Create a New Project or select existing project.

.. figure:: Images/select_project.jpeg
   :alt: Select Project

3. Switch to Credentials. Click on Create Credentials button and Select OAuth Client ID

.. figure:: Images/create_client.jpeg
   :alt: Create OAuth Client

4. Select "Web Application" option and set Authorized redirect URIs "https://developers.google.com/oauthplayground"

.. figure:: Images/create_client_2.jpeg
   :alt: Create OAuth Client

5. This will generate Client ID and Client Secret Key.

.. figure:: Images/client_id_secret_key.jpeg
   :alt: Client ID & Secret Key

6. Switch to Library and search for "Google Drive API". Select API and enable it.

.. figure:: Images/enable_api.jpeg
   :alt: Enable API

7. Now go to https://developers.google.com/oauthplayground/

8. Set your Client ID and Secret Key in Settings

.. figure:: Images/set_id_secret_key.jpeg
   :alt: Set ID and Secret Key in Settings

9. Go to Step 1 section. Search & expand Drive API v3. Select all options and click on Authorize APIs button.

.. figure:: Images/authorize_apis.jpeg
   :alt: Authorize APIs

10. Now, go to Step 2 and there you can find your Refresh token.

.. figure:: Images/refresh_token.jpeg
   :alt: Refresh Token

Once you complete these steps, you will have Google Client ID, Secret Key and Refresh Token for your account. You have to set those in Global Settings of EXT:ns_googledocs.