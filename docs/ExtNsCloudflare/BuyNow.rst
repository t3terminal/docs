.. include:: ../Includes.txt

.. _faq:

==================
Get This Extension
==================

Get this extension from https://t3planet.com/typo3-cloudflare-extension or https://extensions.typo3.org/extension/ns_cloudflare


