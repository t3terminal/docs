.. include:: ../Includes.txt

.. _faq:

=============
Customization
=============

For all customization related details, please refer here : https://docs.t3planet.com/en/latest/ExtThemes/Customization/Index.html

