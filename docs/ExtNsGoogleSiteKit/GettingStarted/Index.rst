.. include:: ../Includes.txt

==================
Getting Started
==================

- `Installing and setting up Site Kit <https://sitekit.withgoogle.com/documentation/getting-started/install>`_
- `Who is Site Kit for? <https://sitekit.withgoogle.com/documentation/getting-started/who-is-site-kit-for>`_
- `Connecting Google services to Site Kit <https://sitekit.withgoogle.com/documentation/getting-started/connecting-services>`_
