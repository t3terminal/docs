.. include:: ../Includes.txt

.. _faq:

================================
Download this awesome extension
================================

Get the Latest Version of this extension with more features and free-support from - `Site Kit by Google for TYPO3 <link https://t3planet.com/sitekit-by-google-for-typo3>`_

Get the Free Version from - https://extensions.typo3.org/extension/ns_google_sitekit
