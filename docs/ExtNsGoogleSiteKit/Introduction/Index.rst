.. include:: ../Includes.txt

=============
Introduction
=============

EXT:ns_google_sitekit
======================

.. figure:: Images/ext_banner.jpg
   :alt: Extension Banner 
   

What is a TYPO3 Google Site Kit?
================================

The TYPO3 Google site kit is a powerful tool that integrates with typo3. It lets you connect and manage Google's services like analytics, search console, page speed insights, and tag manager directly from your TYPO3 backend. This kit simplifies monitoring your website’s performance and user interactions, providing comprehensive insights.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/sitekit-by-google-for-typo3
	- Backend Demo: https://t3-extension-live.t3planet.com/typo3/?TYPO3_AUTOLOGIN_USER=editor-google-sitekit
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support
	- Read Official Docs: https://sitekit.withgoogle.com/documentation/