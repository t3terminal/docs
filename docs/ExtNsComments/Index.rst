﻿.. include:: Includes.txt

===================
EXT:ns_comments
===================

.. toctree::
   :glob:

   Introduction/Index
   Installation/Index
   UpdateVersion/Index
   GlobalConfiguration/Index
   CommentPluginSettings/Index
   CommentModeration/Index
   CommentOnAllPages
   Support
   BuyNow
   