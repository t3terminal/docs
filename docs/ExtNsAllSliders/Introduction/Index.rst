
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_all_sliders
==================

.. figure:: Images/TYPO3-EXTns_all_sliders-Banner-min.jpg
   :alt: TYPO3 EXT ns_all_sliders Banner
   :width: 1000px
   :height: 450px

.. _What-does-it-do:

What does it do?
================

One of the only TYPO3 extension which provides to use most popular jQuery slider plugins at your website. This TYPO3 extension provides to configure sliders eg., **Nivo Slider, Royal Slider, Owlcarousel, Sliderjs Slider** & more will be available in an upcoming version.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/slider-typo3-extension 
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-all-sliders
	- Front End Demo: https://demo.t3planet.com//t3t-extensions/all-sliders
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support