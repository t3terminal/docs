.. include:: ../Includes.txt



Add Events
***********

Now, you need to create events to display in frontend.

You can do it by performing following steps:

**Step 1:** Create a Storage Folder for Events.

**Step 2:** Create events in above storage folder.

.. figure:: Images/Add_event.png
   :alt: Add event

Now you can fill all the details of event.

Before creating events you need to add below Records a you need to select it while creating events.

====================
Add Contact person
====================

.. figure:: Images/Contact_Person.png
   :alt: Contact_Person

=============
Field Group
=============

.. figure:: Images/Field_group.png
   :alt: Field
===========
Organizer
===========

.. figure:: Images/Organizer.png
   :alt: Organizer

==========
Speakers
==========

.. figure:: Images/Speaker.png
   :alt: Speaker

==========
Sponsors
==========

.. figure:: Images/Sponsors.png
   :alt: Sponsor

======
Topic
======

.. figure:: Images/Topic.png
   :alt: Topic


Create event
*************

While creating events you will see multiple tabs, let's show one by one how it works,

General
==========

.. figure:: Images/Event_1.png
   :alt: Event

- **Title** -> Add event title

- **Start date** -> Add Event Start date

- **End date** -> Add Event End date

.. figure:: Images/Event_2.png
   :alt: Event

- **Teaser** -> Add event teaser text

- **Event Description** -> Add Event description

- **Poster image** -> Upload Event poster image

- **Gallery** -> Add more Photos of event,it will show in Frontend.


Location
===============

.. figure:: Images/Event_3.png
   :alt: Event

- **Address** -> Add address of event

- **Latitude** ->add Latitude

- **Longitude** ->add Longitude

- **Include Map** ->By enabling this you can see map in frontend


Configuration
================

.. figure:: Images/Event_4.png
   :alt: Event

- **Online platforms** ->If Event is Online then add platform

- **Link** ->Add link of platform for Online event

.. figure:: Images/Event_4.png
   :alt: Event
   
**Select Contact Person,Organizer,Sponsors,Speaker and topic for event from this tab**

Enable Registration
=========================

.. figure:: Images/Event_6.png
   :alt: Event

- **Enable Registration form** -> While enabling this Registration form will show in event detail page and User can register for event.

- **Enable custom Setup** ->While enabling this Cutom registration Setup will be enabled.

- **Enable Registration Date Range** -> By enabling this custom registration can be define.

- **Registration starts** -> Define Date and time from when registration will start.

- **Registration Ends** -> Define Date and time When registration will end.

- **Enable Custom Message** -> By enabling this Cutom Messages can be define.

- **Message Before Registration Opens** -> This Message will show Before registration starts.

- **Message After Registration Closed** -> This Message will Show After registration ends.

Event Registration
====================

.. figure:: Images/Event_Registration.png
   :alt: Event

You can see User details,who registred for events on FE!

Category
=========

.. figure:: Images/Categories.png
   :alt: Event

You can select Categories for event from this tab.

All Set,Now you can see your added events in Front end!