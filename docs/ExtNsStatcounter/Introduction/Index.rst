
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_statcounter
==================

.. figure:: Images/TYPO3_API_StatCounter_NITSAN_Banner_Preview.jpg
   :alt: TYPO3 EXT TYPO3_API_StatCounter_NITSAN_Banner_Preview Banner

.. _What-does-it-do:

What does it do?
================

StatCounter is a free web traffic analysis service, which provides summary stats on all your traffic and a detailed analysis of your last 500 page views. This limit can be increased by subscribing to their paid service.

The StatCounter TYPO3 Extension brings you all the powerful StatCounter features to your TYPO3 site. `http://statcounter.com/features/ <http://statcounter.com/features/>`_

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/statcounter-typo3-extension-free
	- Front End Demo: https://demo.t3planet.com//t3t-extensions/statcounter
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support