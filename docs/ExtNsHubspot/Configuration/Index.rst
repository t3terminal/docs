.. include:: ../Includes.txt

=============
Configuration
=============

Please follow below step to configuration,

- **Step 1:** Go to Admin Tools Settings.

- **Step 2:** Click to Configuration extension.

.. figure:: Images/config_1.png
   		:alt: Configuration extension

.. figure:: Images/config_2.png
   		:alt: Configuration extension

.. figure:: Images/config_3.png
   		:alt: Configuration extension

- **Step 3:** Click on ns_hubspot.

- **Step 4:** Add Portal Id.

- **Step 5:** Add API Key ( Client Secret Key ).

- **Step 6:** Add Access Token.

Pleae follow below steps for hubspot configuration.

HubSpot Configuration
=======================

- **Step 1:** Create your new account in HubSpot: https://app.hubspot.com/

- **Step 2:** Click on setting icon, and go to Account Setup > Integration > Private Apps > click on Auth

.. figure:: Images/config_4.png
   		:alt: Configuration

- **Step 3:** Get your “Portal Id ”, "API Key(Client secret)" and “Access token” from the Account set up. 

.. figure:: Images/config_5.png
   		:alt: get api

**Generate API Key (Client secret) and Access Token, here is referance link for step by Step guidance https://legacydocs.hubspot.com/docs/faq/how-do-i-create-an-app-in-hubspot**

**After API Key, Portal Id and Access Token generation, configure it in** **settings**

HubSpot Dashboard
==================

After all configuration,You can See Backend Different modules,Log into your hubspot account from Dashboard. 

.. figure:: Images/Login.png
   		:alt: dashboard

After login you can see all configurations of Live chat, Forms and Contacts. Check below screenshot.

.. figure:: Images/dashboard.png
   		:alt: dashboard


Create your HubSpot form
=========================

Create your hubspot form in forms modual:

.. figure:: Images/forms.png
   		:alt: Create forms


Create Website Chatflows in Live Chat
=======================================

Go to HubSpot Live Chat module and click to create chatflow button

.. figure:: Images/live_chat.png
   		:alt: live chat


Add the HubSpot form Plugin
============================

To configure hubspot Form in your site,please follow below steps,

- **Step 1:** Go to page

- **Step 2:** Open Create Content element Wizard 

- **Step 3:** Go to plugin tab add HubSpot form Plugin

.. figure:: Images/Wizard.png
   		:alt: plugin

- **Step 4:** Choose Hubspot Form from dropdown which you want to Integrate.

.. figure:: Images/plugin.png
   		:alt: plugin

Save Configurations and use Plugin as per your requirements.