
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_protect_site
===================

.. figure:: Images/ext_protect_site_banner.jpg
   :alt: Extension Banner 


What does it do?
================

Authentication at TYPO3 - simple though sufficiently powerful feature that provides Administrators & TYPO3 back-end users a way to restrict accessibility of any page with only users having password can access page.

Each page of your website can have different password protection which makes it very much protected from unauthenticated access. Also, page to enter password is very Clean, specific and user-friendly. You can setup it as per your wish.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/password-protected-typo3-pages-free
	- Front End Demo: https://demo.t3planet.com/t3-extensions/protect-site
	- Typo3 Back End Demo : https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/index.php?route=%2Fmain&token=dda676dee97ebcc35373f8a86b89e0fa8952e2e2
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support