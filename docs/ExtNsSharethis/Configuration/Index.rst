
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Quick & Easy "Global" configuration of Sharethis.com
====================================================


Global Configuration
---------------------

.. rst-class:: ol-bignums

#. Go to the Setting module & click on the Configure Extension, now you will see the ns_sharethis settings.

#. Enable Global Interface into this **settings** Take a look at screenshots as follows.

   	.. figure:: Images/TYPO3-EXTns_sharethis-Config1.jpeg
   		:alt: Config_1-1
   		:width: 1100px

Setup "Particular Page Only" configuration of Sharethis.com
------------------------------------------------------------

.. rst-class:: ol-bignums

#. Go to the Setting module & click on the Configure Extension, now you will see the ns_sharethis settings and then **Disable Global Interface** into this *settings*.

#. Now add extension Above/Below page as per your requirement.

#. Now select plugin **Nitsan Social widget** and configure it as per requirement, Checkout this screenshot.


	.. figure:: Images/TYPO3-EXTns_sharethis-ConfigDisable.jpeg
	   :alt: Config1
	   :width: 1100px

	.. figure:: Images/TYPO3-EXTns_sharethis-Config2.jpeg
	   :alt: Config2
	   :width: 1100px

	.. figure:: Images/TYPO3-EXTns_sharethis-Config3.jpeg
	   :alt: Config3
	   :width: 1100px


.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches'
from the top panel. The 'Clear cache' function of the install tool will also
work perfectly.
