
.. include:: ../Includes.txt

============
Introduction
============

ns_theme_t3reva
================

.. figure:: Images/t3reva_banner.png
   :alt: t3planet_theme_t3reva_banner
      		 
T3 Reva Typo3 Headless template designed using power of React and Nextjs,its a fusion of cuttind-edge design and performance optimization

Extensions Dependencies
=======================

- ns_basetheme
- news
- headless
- headless_news

**Note:** During the installation, You need to install EXT: headless_news manually from the github. Please check it from here - https://github.com/TYPO3-Initiatives/headless_news


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/t3-reva-reactjs-typo3-template
	- Front End Demo: https://demo.t3planet.com/?theme=t3t-reva
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support