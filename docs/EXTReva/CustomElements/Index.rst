.. include:: ../Includes.txt

.. |true-icon| image:: Images/righ-icon.png
.. |false-icon| image:: Images/crose-icon.png
.. |view-icon| image:: Images/view.png
.. |version-icon| image:: Images/version.png
.. |documentation-icon| image:: Images/documentation.png
.. |typo-icon| image:: Images/typo.png


.. _Action_And_Results:

================
Mask Elements
================

Template's Elements
===================

Whenever you are going to add a new element, in wizard you can find "Mask Elements" tab where template related custom elements been configured.


.. figure:: Images/Custom_Element.png
   :alt: Custom Elements

.. figure:: Images/Custom_element1.png
   :alt: custom_elements1

.. figure:: Images/Custom_element_2.png
   :alt: custom_elements1
