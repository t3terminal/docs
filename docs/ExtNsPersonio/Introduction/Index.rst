
.. include:: ../Includes.txt

=============
Introduction
=============


EXT:ns_personio
===================

.. figure:: Images/Personio_Banner.jpg
   :alt: Extension Banner 


What does it do?
================

ns_personio is TYPO3 extension that brings Personio job vacancies right to your website, With Personio Recruting api you can make your Recruitment process effortlessly from your website! 

Helpful Links
=============

.. Note::

	- Product:https://t3planet.com/typo3-personio-extension
	- Front End Demo: https://demo.t3planet.com//t3t-extensions/ns-personio
	- Typo3 Back End Demo : https://demo.t3terminal.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-ns-personio
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support