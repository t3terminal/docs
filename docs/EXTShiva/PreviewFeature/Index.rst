.. include:: ../Includes.txt

===============
Preview Feature
===============

Whenever you change the back-end content and are eager to view in front-end, we have implemented "Preview Feature" feature of ReactJS/NextJS framework.

Take a look at documentation https://nextjs.org/docs/advanced-features/preview-mode

To swtich to preview mode simpy call `api/draft?slug=<slug>`` from Frontend.

.. Note:: Inbuilt, We have configured Nextjs preview feature with link e.g., /api/preview/. So that whole your site is in preview mode by default.