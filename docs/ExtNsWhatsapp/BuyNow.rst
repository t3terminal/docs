.. include:: ../Includes.txt

.. _whatsapp:

==================
Get This Extension
==================

Get Latest Version of this extension with more-features and free-support from https://t3planet.com/typo3-whatsapp-extension or for free version https://extensions.typo3.org/extension/ns_whatsapp
