.. include:: ../Includes.txt

===============
Global Settings
===============

Once you install this extension, Your first-step should be to configure all the settings from "Global Configuration".

**Step 1.** Go to Nitsan Module > NS Whatsapp

**Step 2.** Click on "Chat / Share / Group" menu, Fill-up Whats app number, Click on the enable checkbox & insert all the required information and click on "Save Changes" button.

**Step 3.** Click on "Style" menu, Give your own style to whatsapp widget & click on "Save Changes" button.


.. Note:: All this configurations can be saved from Typo3 Constant Editor also.


.. figure:: Images/ns-whatsapp-share-typo3-global-settings.jpeg
   :alt: ns-whatsapp-group-typo3-global-settings 
   
.. figure:: Images/ns-whatsapp-share-typo3-global-settings.jpeg
   :alt: ns-whatsapp-group-typo3-global-settings
   
.. figure:: Images/ns-whatsapp-Style1-typo3-global-settings.jpeg
   :alt: ns-whatsapp-Style1-typo3-global-settings

.. figure:: Images/ns-whatsapp-Style2-typo3-global-settings.jpeg
   :alt: ns-whatsapp-Style2-typo3-global-settings
   
.. figure:: Images/ns-whatsapp-Style3-typo3-global-settings.jpeg
   :alt: ns-whatsapp-Style3-typo3-global-settings