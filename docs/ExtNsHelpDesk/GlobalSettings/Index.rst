.. include:: ../Includes.txt

===============
Global Settings
===============

Once you install this extension, Your first-step should be to configure all the settings from "Global Configuration".

**Step 1.** Go to Admin Tools > Helpdesk Configurations

**Step 2.** Click on "Global Settings" menu, Fill-up all the information and click on "Save Changes" button.

.. figure:: Images/ns-helpdesk-typo3-global-settings1.png
   :alt: ns-helpdesk-typo3-global-settings1
   
.. figure:: Images/ns-helpdesk-typo3-global-settings2.png
   :alt: ns-helpdesk-typo3-global-settings2
   
.. Note:: If you will not enable the automatic ticket assignment field then category wise assignee would not be work. so you must enable that field as well as you should have the categories on your helpdesk extension.