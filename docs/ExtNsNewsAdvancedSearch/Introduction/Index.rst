
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_news_advancedsearch
==========================

.. figure:: Images/typo3-ext-advance-search-for-ext-news.jpg
   :alt: Extension Banner 


What does it do?
================

This plugin extends your search results and allows to search by content with filters in fields created using Advanced plugin. This plugin use specially designed Search algorithms for that purpose, which gives you both speed and relevance. 
Everything is fully configurable in the TYPO3 settings for the plugin. Also available for TYPO3 MU (multisites).


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/advanced-search-typo3-news-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-news-advancedsearch
	- Front End Demo: https://demo.t3planet.com//t3t-extensions/news-advancedsearch
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support