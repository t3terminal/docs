.. include:: ../Includes.txt

.. _installation:

====================
License De-Activation
====================

Why De-Activate License Key?
============================

We have the flexibility to use your license key to your any TYPO3 site/domain. Whenever you want to switch from one domain/site to another, you can use the "De-activate License" feature. To change your domain for your purchased product, you don't need to contact us - Just de-activate the license and use re-use at your new domain.


How to De-Activate License Key?
===============================

To de-activate or de-register your purchased license key from your TYPO3 instance, You can follow these steps.

**Step 1.** Login to your TYPO3 Instance

**Step 2.** Switch to NITSAN > License Management.

**Step 3.** Click on "Deactivate License" of your particular purchased TYPO3 product.

.. figure:: Images/LicenseDeActivate.jpeg
   :alt: De-Activate License

**Step 4.** Go to Admin Tools > Extensions > De-Activate Your Installed Extension.

.. figure:: Images/InactiveExtension.jpeg
   :alt: In-Active Extension

.. Attention::
   Now, you can use this license key to your another TYPO3 instance because it's been de-activated from your TYPO3 instance.
