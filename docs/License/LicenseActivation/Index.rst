.. include:: ../Includes.txt

.. _installation:

==================
License Activation
==================


Install via Extension Manager
=============================

**Step 1.** Go to Extension module & Select “Get extensions” from the drop-down at top. Click on the Update Now button to get the Extension Repository.

.. figure:: Images/update-ext-repository.jpeg
   :alt: Update Extension Repository

**Step 2.** Now, Download the ns_license extension & install it.

.. figure:: Images/DownloadExtension.jpeg
   :alt: DownloadExtension

**Step 3.** Switch to NITSAN > License Management > Add Your License Key.

.. figure:: Images/LicenseModule.jpeg
   :alt: License Management

.. figure:: Images/LicenseActivated.jpeg
   :alt: License Activated

**Step 4.** Go to Admin Tools > Extensions > Activate Your Purchased Extension.

.. figure:: Images/InstallExtension.jpeg
   :alt: Install Extension


Install via Composer
====================

**Step 1.** Install EXT:ns_license

.. code-block:: python

   composer require nitsan/ns-license

   vendor/bin/typo3 extension:setup

**Step 2.** Go to TYPO3 Backend > License Manager > Add Your License Key.

.. figure:: Images/LicenseModule.jpeg
   :alt: License Management

.. figure:: Images/LicenseActivated.jpeg
   :alt: License Activated

**Step 4.** Run Composer Command

.. code-block:: python

   composer config repositories.nitsan '{
      "type": "composer",
      "url": "https://composer.t3planet.cloud",
      "only": ["nitsan/<PACKAGE-NAME>"]
   }'

.. code-block:: python

   composer config http-basic.composer.t3planet.cloud <USERNAME> <LICENSE-KEY>

.. code-block:: python

   composer req nitsan/<PACKAGE-NAME> --with-all-dependencies

.. code-block:: python

   vendor/bin/typo3 extension:setup

.. note:: We have already sent the license key & composer credentials (like username, license key) via Email. If you need any help, then write to our support team https://t3planet.com/support

.. attention:: If you are installing EXT:ns_revolution_slider with TYPO3 >= v11 composer-based TYPO3 instance, Please don't forget to run the below commands.

.. code-block:: python

   vendor/bin/typo3 nsrevolution:setup
