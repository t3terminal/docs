.. include:: ../Includes.txt

.. _faq:

==================
Get This Extension
==================

Get this extension from https://extensions.typo3.org/extension/ns_wp_migration/ or https://t3planet.com/typo3-wp-migration-extension
