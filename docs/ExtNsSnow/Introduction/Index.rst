
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_snow
===========

.. figure:: Images/ext-snow-banner.jpg
   :alt: Extension Banner 


**This Christmas, Let it snow with our extension ns_snow!**

Christmas time? Do you want to add traditional snowfall to your TYPO3 site? Just use this plug-n-play TYPO3 extension with easy to use backend configuration.

What does it do?
================

Spread the cheers of winter by adding beautiful snowflakes and snowfall to your website.

Christmas Snow spreads the cheer of Christmas by adding beautiful snowflakes and snow-fall animation to your TYPO3 website with our little addon ns_snow and all the pages will show falling flakes of snow!

To make up for the lack of snow in our virtual world, you should see the snowflakes slowly zigging and zagging as they fall from the virtual sky!

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/snowfall-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-snow
	- Front End Demo: https://demo.t3planet.com/t3-extensions/snow
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support