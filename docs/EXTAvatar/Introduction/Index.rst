
.. include:: ../Includes.txt

============
Introduction
============

ns_theme_t3avatar
=================

.. figure:: Images/Theme_T3Avatar.png
   :alt: ns-theme-t3avatar-banner
   
About Our TYPO3 Templates
=========================

- Stable version
- Pre-configured
- Flexible Backend Management
- Fast, Lightweight & Powerful
- Optimized and Extendable
- Future Upgradable
- Followed TYPO3 Core Standards
- Highly compatible with TYPO3 extensions


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/t3-avatar-multipurpose-typo3-template
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3-avatar/typo3/?TYPO3_AUTOLOGIN_USER=editor
	- Front End Demo: https://demo.t3planet.com/?theme=t3-avatar
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support