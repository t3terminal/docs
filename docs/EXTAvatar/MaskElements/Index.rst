.. include:: ../Includes.txt

.. |true-icon| image:: Images/righ-icon.png
.. |false-icon| image:: Images/crose-icon.png
.. |view-icon| image:: Images/view.png
.. |version-icon| image:: Images/version.png
.. |documentation-icon| image:: Images/documentation.png
.. |typo-icon| image:: Images/typo.png


.. _Action_And_Results:

================
Mask Elements
================

Whenever you are going to add a new element, in wizard you can find "Mask Elements" tab where template related mask elements been configured. All the Mask Elements are self explanatory.

.. figure:: Images/Mask_element.png
   :alt: Mask Elements
   
.. figure:: Images/Mask_element1.png
   :alt: Mask Elements
   
.. figure:: Images/Mask_element2.png
   :alt: Mask Elements