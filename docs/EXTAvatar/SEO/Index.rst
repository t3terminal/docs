.. include:: ../Includes.txt

.. |true-icon| image:: Images/righ-icon.png
.. |false-icon| image:: Images/crose-icon.png
.. |view-icon| image:: Images/view.png
.. |version-icon| image:: Images/version.png
.. |documentation-icon| image:: Images/documentation.png
.. |typo-icon| image:: Images/typo.png


.. _Action_And_Results:

================================
SEO (Search Engine Optimization)
================================


1. Sitemap
===========

You can access Sitemap of template by appending sitemap.xml at the end of BaseURL of template.

For example: https://www.domain.com/sitemap.xml

We have already take care during installation of Template but, Please make sure you've already settings from Site Management > Sites > Edit ns_theme_t3avatar > Static Routes

.. figure:: Images/T3Avatar_staticroutes.png
   :alt: T3Planet-TYPO3-SEO
   :width: 1300px


2. Robots.txt
==============

You can access Robots.txt of template by appending robots.txt at the end of BaseURL of template

For example: https://www.domain.com/robots.txt


3. Set Meta Description & Tags
==============================

Set Meta Description and Tags of any page from Metadata tab in page properties. Check below screenshot.

.. figure:: Images/metadata.png
   :alt: Set metadata
   :width: 1300px

4. Setup OG tags for Social Media
=================================

When any page is shared on Social Platforms like Facebook, Twitter, OG tags setup for that page will be used for page preview. Set them as below.

.. figure:: Images/social_media.png
   :alt: Set Social media
   :width: 1300px

5. Setup 404 page
=================

You will get pre-configured 404 page with the template. You can access and modify it from Root Page > Main Menu > Pages > Extra > 404 - Page Not Found. Check below image for more info.

.. figure:: Images/404.png
   :alt: 404 Page

We have already take care during installation of Template but, Please make sure you've already settings from Site Management > Sites > Edit ns_theme_t3avatar > Error Handling

.. figure:: Images/T3Avatar_errorhandling.png
   :alt: T3Planet-TYPO3-404
   :width: 1300px