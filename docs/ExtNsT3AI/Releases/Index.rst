﻿.. include:: ../Includes.txt

========
Releases
========

T3AI TYPO3 KI Extension is created to simplify your daily TYPO3 backend tasks. We're dedicated to providing the best features and improvements for your T3AI extension. Check out the version history of our most popular and loved TYPO3 AI extension.

Change log
==========

.. raw:: html

    <style>
        .custom-table caption {
            text-align: left !important;
            font-size: 1em !important;
            font-style: normal !important;
        }
        .custom-table {
            border: 1px solid black;
            border-collapse: collapse;
            width: 100%;
        }
        .custom-table th, .custom-table td {
            border: 1px solid #C8D4E3;
            padding: 8px;
            text-align: left;
            white-space: normal !important;
        }
        .custom-table th {
            background-color: #edf3f9;
            color: #04122D;
        }
    </style>


.. list-table:: **4.0**
   :widths: 20 50
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Feature 
     - Completely revamped the T3AI extension 
   * - Feature 
     - New UI/UX (using TYPO3 core only) for the best backend usability
   * - Feature 
     - Added new custom backend modules
   * - Feature 
     - AI SEO: Re-form All-in-One SEO, SEO Meta Data, Social Media Meta.
   * - Feature 
     - AI SEO: New SEO - Start working on a new page, define an AI-driven SEO strategy by crafting titles, meta keywords, descriptions, OG meta data, etc.
   * - Feature 
     - AI SEO: SERP Snippet - Preview your website snippet to see your Google listing. Optimize title, description, and content to attract more visitors.
   * - Feature 
     - AI SEO: Content Analysis - Analyze your page text and headings, and Get AI recommendations to make your content better and reachable to your global audience. 
   * - Feature 
     - AI SEO: Content Analysis - Analyze your page text and headings, and Get AI recommendations to make your content better and reachable to your global audience.
   * - Feature 
     - AI SEO: SEO Page Score - Easily check your website's SEO score. See how you can improve your ranking and get recommendation.
   * - Feature 
     - AI Pages: Page/Blog (Simple) - Quickly build an AI-powered Page using your page titles, content, meta data, AI-image and elements with a Single-click!
   * - Feature 
     - Page/Blog (Advanced) - Build an advanced AI-powered Page by selecting topics and creating an outline, mutipile elements/images in Few-clicks!
   * - Feature 
     - AI Pages: Page Tree - This feature allows you to automatically create a hierarchical structure of web pages/menus using AI.
   * - Feature 
     - AI Pages: News (Simple) - Generate compelling News articles with just a few steps. Simply add topics, keywords, and types of news.
   * - Feature 
     - AI Pages: News (Advanced) - Create a sophisticated AI-driven News platform by choosing topics, outlining stories, integrating mutipile elements/images in Few-clicks!  
   * - Feature 
     - AI Content/News/Blog: Content Rewriter - Create an original articles and contents with the assistance of our online AI content rewriting tool in just a few seconds.
   * - Feature 
     - AI Content: Content Elements (Create/Edit) - Next-level AI, You can now generate TYPO3 core elements using advanced AI, along with custom content elements and blocks that you design. 
   * - Feature 
     - AI Content: Content Translate - Instantly translate content across multiple languages with our advanced AI-powered Content AI translate feature.  
   * - Feature 
     - AI Content: Content Fields - Start generating AI-driven content for all your content element's fields (such as textboxes and textareas) by writing your prompt.
   * - Feature 
     - AI Content: RTE AI Assistant - Get instant help generation content with advanced AI. The RTE assistant helps you create engaging content directly in your TYPO3 RTE.
   * - Feature 
     - AI Translation: Language Glossary - Use our Global Glossary to translate and replace terms. Just add the text you want to replace, and it will be translated accurately. 
   * - Feature 
     - AI Translation: Re-Translate Pages - Simply click the "Re-Translate" button. It will remove translated pages and initiate the language wizard to begin the translation process.
   * - Feature 
     - AI Translation: News Translation - Expand your knowledge base with News Translations. Click 'Translate with T3AI' to get translated news directly in your backend. 
   * - Feature 
     - AI Translation: Records TCA Translation - Transform your records and data into various languages using an intuitive feature for records and data translation. 
   * - Feature 
     - AI Translation: Activate Translated Content - By default, translated content is disabled. Simply click the "Activate Translated Content" button to enable it.
   * - Feature 
     - AI Translation: (re-formed) Translate Pages - Instantly translate entire webpages into different languages. Make your website accessible to a global audience with just a few clicks.
   * - Feature 
     - AI Translation: Extensions Localization (XLF) - Make your TYPO3 extensions for a global audience with easy-to-use XLF file AI translations for easy localization. 
   * - Feature 
     - AI Media: DALL-E AI Images - Generate AI-powered images with a single click using the powerful integration of DALL-E. 
   * - Feature 
     - AI Media: MidJourney - Create high-quality images from simple text prompts for free. Just enter a text description of the image and get the awesome images.
   * - Feature 
     - AI Media: Stability AI Media - Create stunning images, videos, and audio with advanced AI technology. Stability AI media can do it with one click!
   * - Feature
     - AI Media: AI Audio - Simply add your script or text and create high-quality AI audio in seconds with the powerful AI audio features.
   * - Feature
     - AI Media: Unsplash, Openverse, Pixabay, Pexels - Explore a diverse collection of AI images for any project or theme integrated. 
   * - Feature 
     - AI Prompts: Chat Assistance - Engage in natural conversations with the Improved ChatGPT Assistant in TYPO3 backend for quick answers and content insights.
   * - Feature 
     - AI Prompts: Helpful Prompts - Access hundreds of pre-defined prompts to meet all your AI requirements. Select your preferred prompt, customize it, and execute effortlessly.
   * - Feature 
     - AI Prompts: Helpful Prompts Manage - Manage and organize your AI prompts with Helpful Prompts Manager. Streamline workflow and enhance productivity by integrating.
   * - Feature 
     - AI Prompts: Sidebar AI Assistant - Make work easier with ChatGPT sidebar! Simplify your workflow with features like summarization, creation, and text rewriting.
   * - Feature 
     - AI Prompts: Sidebar AI Prompts - Create your own custom Sidebar AI Prompts with Prompt manager. Simply edit and add your own prompts to Sidebar AI.
   * - Feature 
     - AI Prompts: SEO/Pages/Content/Translation/Media Prompts - Organize and manage your  prompts efficiently with our Prompts Manager. Keep all your creative ideas in one place, easily accessible and ready for use
   * - Feature 
     - AI Settings: Feature Toggles and settings for all the backend modules.
   * - Feature 
     - AI Techies: Report Issues - Facing issues with the T3AI TYPO3 Extension? Report them using the issue icon. 
   * - Feature 
     - AI Techies: Suggest Features - Have suggestions or feedback for the T3AI TYPO3 Extension? Submit your feature requests. 
   * - Feature
     - AI Techies: TYPO3 AI Chatbot - Are you seeking help with TYPO3 code as a developer or integrator? Try our custom-made TYPO3 AI Chatbot for assistance.
   * - Task 
     - Ensured compatibility with TYPO3 versions v11, v12, v13
   * - Task 
     - Passed automated code lint checks (PHP, TS, etc.) 
   * - Task 
     - Verified cross-browser and cross-device compatibility
   * - Task
     - Followed the TYPO3 standard checklist
   * - Task
     - Conducted code clean-up and optimizations
   * - Bugfix
     - Performed comprehensive testing and bug-fixing
   * - Release
     - Major breaking changes release v4.0.0


.. list-table:: **3.1.3**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Resolve the Issue with a Helpful Prompt Generation
   * - BugFix
     - Code cleanup and code improvements
   * - Release
     - Bug-fix version release 3.1.3




.. list-table:: **3.1.2**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Configure Language Speech in Site Configuration
   * - BugFix
     - Code cleanup and code improvements
   * - Release
     - Bug-fix version release v3.1.2



.. list-table:: **3.1.1**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Resolve the Issue with ButtonBar
   * - BugFix
     - Code cleanup and code improvements
   * - Release
     - Bug-fix version release 3.1.1



.. list-table:: ** 3.1.0**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Feature 
     - GPT-4o LLM Model Integration: Leverage the latest GPT-4o language model for enhanced AI capabilities
   * - Feature 
     - Generative AI Image Support: Generate images using OpenAI's DALL-E.
   * - Feature 
     - AI Images Module Enhancements  
   * - Feature 
     - Listing Options: Choose between card and table layouts.
   * - Feature 
     - Search & Sorting: Efficiently find and organize AI-generated images. 
   * - Feature 
     - Image Generation Features.
   * - Feature 
     - Prompt-Based Image Creation: Generate images based on user prompts.
   * - Feature 
     - Resolution Flexibility: Create images in various resolutions
   * - Feature 
     - Image Variants: Produce different versions of images
   * - Feature 
     - Variant Resolutions: Generate image variants in multiple resolutions  
   * - Feature 
     - Content Element Integration: Embed AI-generated images directly into content elements.
   * - Feature 
     - AI-Driven Suggestions: Get intelligent suggestions for all extension records and data.
   * - Feature 
     - Automated AI Content Creation: Automatically create content with AI pages. 
   * - Feature 
     - Glossary Translation Feature: Introduce a glossary tool for translation purposes.
   * - Feature 
     - German Language Support: Full backend support for the German language.
   * - Feature 
     - New Translation Wizard Option: Use 3AI for auto-detect translation functionality.
   * - Feature 
     - Feature Management: Enable or disable features directly from the extension settings. 
   * - Feature 
     - Translation Status Options: Backend option for AI translation status and translation date in page properties.
   * - Feature 
     - Frontend Translation Display: Show translation status and date on the frontend 
   * - Task 
     - New Logo: Updated the extension with a new logo 
   * - Task 
     - Deprecation Removal: Eliminated deprecated and breaking changes
   * - Task 
     - Code Improvements: Performed extensive code cleanup and improvements. 
   * - Task 
     - Version Compatibility: Enhanced compatibility with newer TYPO3 versions
   * - Task 
     - Automatic Code Review: Implemented automated code review tools such as PHP-lints and TypoScript-lints
   * - Bugfix
     - Functional Testing: Conducted rigorous functional testing and bug fixes.
   * - Bugfix
     - UI/UX Design Testing: Fixed compatibility issues and improved the design for a better user experience.
   * - Bugfix
     - Quality Assurance: Completed comprehensive QA testing and bug fixing
   * - Release
     - Feature version release v3.1.0



.. list-table:: **3.0.2**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Improved the error messages
   * - BugFix
     - Improved prompt and make it bugs free
   * - BugFix
     - Issue with the Backend User Group Module access is fixed
   * - Release
     - Minor version release 3.0.2



.. list-table:: **3.0.1**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Improve the Backend Module Icon and Extension Icon
   * - BugFix
     - Fixed the issue with Two Letter ISO issue
   * - Release
     - Release minor version 3.0.1

 

.. list-table:: **3.0.0**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Important: Rename extension from EXT:ns_openai to EXT:ns_t3ai
   * - Task
     - Breaking Changes: Uninstall existing EXT:ns_openai & Install EXT:ns_t3ai with your same license key
   * - Task
     - Refactoring and enhancements in code and namespaces
   * - Task
     - Code cleanup and code improvements
   * - BugFix
     - QA testing & bug-fixing
   * - Release
     - Major version release v3.0.0

  
.. list-table:: **2.0.0**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Feature 
     - Launched ChatGPT Sidebar
   * - Feature 
     - Re-developed RTE for real-time ChatGPT chatbots
   * - Feature 
     - Introduced one-click all SEO optimizations
   * - Feature 
     - Enable/Disable language translation wizard
   * - Feature 
     - File metadata translations
   * - Feature 
     - Records language translation 
   * - Feature 
     - Generate automatic SEO meta-data for TYPO3 AI Pages 
   * - Feature 
     - Choose content element type for TYPO3 AI Pages
   * - Feature 
     - Improved chat style at ChatGPT backend module
   * - Feature
     - Editable all optimized SEO results  
   * - Task 
     - Remove deprecations and breaking changes 
   * - Task 
     - Code cleanup and code improvements
   * - Task 
     - Improve TYPO3 Version Compatibility
   * - Task 
     - Automatic code review with PHP-lints, TypoScript-lints, etc
   * - Bugfix
     - Functional Testing and bug fixing
   * - Bugfix
     - UI/UX design and fixed TYPO3 version compatibility
   * - Bugfix
     - QA testing & bug-fixing
   * - Release
     - Major version release v2.0.0



.. list-table:: **1.3.0**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Compatibility with TYPO3 v12
   * - Task
     - Code cleanup and code improvements
   * - Bug Fix
     - QA testing & bug-fixing
   * - Release
     - Minor version release v1.3.0


.. list-table:: **1.2.2**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Bugfix
     - Minor version release v1.2.2
   * - Task
     - Code cleanup and code improvements
   * - Release
     - Minor version release v1.2.2



.. list-table:: **1.2.1**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Bugfix
     - Resolve access & permission issue while creating AI-Pages
   * - Task
     - Code cleanup and code improvements
   * - Release
     - Minor version release v1.2.1



.. list-table:: **1.2.0**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Task
     - Compatibility with TYPO3 v10
   * - Feature
     - Search Feature at Helpful Prompts
   * - Task
     - Code cleanup and code improvements
   * - Release
     - Release of version 1.2.0


.. list-table:: **1.0.0**
   :widths: 25 25
   :header-rows: 1
   :class: custom-table

   * - Change Log
     - Changes
   * - Feature 
     - Premium Features Development of T3OpenAI
   * - Feature 
     - 3-Steps to Launch TYPO3 Pages
   * - Feature 
     - AI-Powered Content via TYPO3 RTE
   * - Feature 
     - Optimise SEO with AI 
   * - Feature 
     - Automize Translation of Your Pages  
   * - Feature 
     - Helpful Prompts at TYPO3 Backend
   * - Feature 
     - ChatGPT Personal Assistant  
   * - Task 
     - Compatibility with TYPO3 v11.x
   * - Task 
     - Support Composer based TYPO3 instance
   * - Task 
     - Compatibility of cross-browsers and cross-devices
   * - Task 
     - Follow TYPO3 standard checklist
   * - Task 
     - Code clean-up and code improvement
   * - Task 
     - Improve speed and performance rank
   * - Task 
     - Automatic code review with Gitlab-CI lints
   * - Bugfix
     - Functional Testing and bug fixing
   * - Bugfix
     - UI/UX design and fixed TYPO3 version compatibility
   * - Bugfix
     - QA Testing & Bug-fixing
   * - Release
     - Release of version 1.0.0










 







     
















