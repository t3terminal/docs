.. include:: ../Includes.txt


=============
Configuration
=============

Before using the extensions, you need to configure them. Please follow the settings below to start your T3AI journey.

Setup Default AI Model
======================
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/showcase/cm1gm7tws00c214kkyqjyrdhz?demo=47 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

To set up the API for your AI models, follow these steps:

- Go to Admin Tools > Settings > Click on "Configure extensions"
- Click on "AI Engine" and setup your default AI-Models
- Click "Save ns_t3ai configuration" button

.. figure:: Images/Configurations.png
   :alt: T3AI_Configuration

.. figure:: Images/Configurations_1.png
   :alt: T3AI_Configuration1


Configure APIs of AI Model
==========================

Next, set up API keys and other information for your desired AI model:

- Go to Admin Tools > Settings > Click on "Configure extensions"
- Click on your choosen AI Model e.g., ChatGPT
- Enter API keys and other informaiton of that AI model
- Click "Save ns_t3ai configuration" button


API Key Generation Links for T3AI
=====================================

Below are the links for generating API keys for various AI models used in T3AI. Please follow the respective links to create and obtain your API keys

- .. raw:: html

   <a href="https://platform.openai.com/docs/api-reference/introduction" target="_blank">ChatGPT</a>

- .. raw:: html

    <a href="https://support.deepl.com/hc/en-us/articles/360020695820-API-Key-for-DeepL-s-API" target="_blank">Deepl</a>
    
- .. raw:: html

    <a href="https://cloud.google.com/docs/authentication/api-keys" target="_blank">Google Translate</a>
    
- .. raw:: html

    <a href="https://ai.google.dev/gemini-api/docs/api-key" target="_blank">Gemini</a>

- .. raw:: html

    <a href="https://docs.anthropic.com/en/api/getting-started#accessing-the-api" target="_blank">Claude</a>
    
- .. raw:: html

    <a href="https://learn.microsoft.com/en-us/azure/search/search-security-api-keys" target="_blank">Azure</a>
    
- .. raw:: html

    <a href="https://help.unsplash.com/en/articles/2511245-unsplash-api-guidelines" target="_blank">Unsplash</a>
    
- .. raw:: html

    <a href="https://api.openverse.org/v1/" target="_blank">Openverse</a>
    
- .. raw:: html

    <a href="https://pixabay.com/api/docs/" target="_blank">Pixabay</a>
    
- .. raw:: html

    <a href="https://docs.midjourney.com/" target="_blank">MidJourney</a>
    
- .. raw:: html

    <a href="https://platform.stability.ai/docs/api-referenc" target="_blank">Stability AI</a>
    
- .. raw:: html

    <a href="https://elevenlabs.io/docs/api-reference/text-to-speech" target="_blank">ElevenLabs</a>
