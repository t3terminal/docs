.. include:: ../Includes.txt

.. _installation:

===================
System Requirements
===================

Here is the pre-requeist to install, configure and use T3AI TYPO3 AI extension.

- PHP v7.4 - v8.3
- TYPO3 v11 - v13
- EXT:backend
- EXT:filelist
- EXT:dashboard
- EXT:fluid
- EXT:seo
- EXT:ns_license