.. include:: ../Includes.txt


=========================
For Developers
=========================

Explore the advanced features of T3AI that can streamline your TYPO3 development process. Whether you're managing content or optimizing performance, T3AI has the tools you need.

TYPO3 AI Chatbot
~~~~~~~~~~~~~~~~

Are you seeking help with TYPO3 code as a developer or integrator? Try our custom-made TYPO3 AI Chatbot for assistance.

Explore T3AI - TYPO3 AI Chatbot
https://chatgpt.com/g/g-MDKrvyZk5-t3ai 


Report Issues and Request Features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you find any issues or want to add any custom feature contact us at `Contact <https://t3planet.com/contact>`_


Extend Records Translation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ns_t3ai supports translation of specific fields of TCA records. It understands fields which need to be translated, only if their l10n_mode is set to prefixLangTitle.

For detecting translatable fields, ns_t3ai uses a DataHandler hook.

The following setup is needed, to get ns_t3ai work on your table:

<extension_key>/Configuration/TCA/Overrides/<table_name>.php

.. code-block:: python

 $GLOBALS['TCA']['<table_name>']['columns']['<field_name>']['l10n_mode'] = 'prefixLangTitle';
 $GLOBALS['TCA']['<table_name>']['columns']['<field_name>']['l10n_mode'] = 'prefixLangTitle';


APIs of T3AI Extension
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the T3AI TYPO3 Extension's APIs, you can extend and customize functionalities according to your needs.

*This feature will be released in the upcoming new version*

AI System Log 
~~~~~~~~~~~~~~~~

Track all AI system activities and monitor how your backend users interact with the AI models,follow below steps to check AI log.

- **Step 1** - Log in to your TYPO3 backend and navigate to the T3AI dashboard.

    - Select the **'Techies'** tab and click on **'AI Log Manager.'**

.. figure:: Images/AIlog.png
   :alt: AI log

- **Step 2** - Open AI Log Manager

.. figure:: Images/Log.png
   :alt: AI log

- **Step 3** - Simply search for the page and select all the required fields from the dropdown menu.

     - **AI Engine** - It displays the types of AI engine logs you want to view.

     - **AI Models** - Select AI Engine Model types. For i.e GPT 4.0

     - **Module** - This Indicates T3AI module Logs. For SEO, Translation.

     - After Selecting Your Required field Click on Filter and your AI model Logs are generated.

.. figure:: Images/Log_1.png
   :alt: AI log

- **Step 4** - You can reset your AI model logs by clicking on the 'Reset' button.
