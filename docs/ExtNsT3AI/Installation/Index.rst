.. include:: ../Includes.txt

.. _installation:

============
Installation
============

Installing the T3AI—TYPO3 Extension is easy. Follow the steps below to add the extension to your TYPO3 environment.

For Free Version
================

To install the free version of the extension, just go to the Extension Manager and search for 'ns_t3ai'.

**Step 1:** Navigate to the "Extension Manager" module.

**Step 2:** Select “Get the extension” from the dropdown menu.

**Step 3:** Search with Extension Key "ns_t3ai" 

**Step 4:** Click on the " Retrieve/Update " Button, to Import Extension From repository. 

Get the latest version of the extension from typo3.org  - `ns_t3ai <https://extensions.typo3.org/extension/ns_t3ai//>`_


For Premium Version - License Activation
============================================

To activate license and install this premium TYPO3 product, Please refer this documentation https://docs.t3planet.com/en/latest/License/Index.html