.. include:: ../Includes.txt

=============
Upgrade Guide
=============

T3AI v1 to v3
=============

Follow our standard update guide https://docs.t3planet.com/en/latest/License/UpdateVersion/Index.html


T3AI v3 to v4
=============

T3AI’s v4 is a completely revamped and redeveloped TYPO3 extension. Please follow the steps below to upgrade:

- Step 1. Deactivate the license key from the License Manager module.

- Step 2. Uninstall and delete the extension.

- Step 3. Consider fresh new installation the extension from https://docs.t3planet.com/en/latest/License/LicenseActivation/Index.html

Synchronize AI Prompts
=======================

We continuously improve AI prompts, refining existing ones or adding new ones with each update. To ensure smooth operation, follow the steps below to synchronize AI prompts

.. figure:: Images/AI_Synchronize.png
   :alt: AI Synchronize

.. attention:: Before updating, back up your database to ensure its safety.

- **Step 1:** Go to Extension Module

- **Step 2:** In installed extension > search "T3Ai"

- **Step 3:** Click on Import DB Button

- **Step 4:** Click on Button **Re-import data**

.. warning:: Please be aware that any custom prompts you’ve created will be overwritten during synchronization. After the process is complete, you’ll need to manually recreate your custom AI prompts. We apologize for the inconvenience

Test the updated T3AI features to ensure everything is working properly :)