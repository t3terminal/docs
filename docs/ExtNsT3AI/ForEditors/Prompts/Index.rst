.. include:: Includes.txt

Prompts
-------

Chat Assistance
~~~~~~~~~~~~~~~

Make your TYPO3 site smarter with T3AI Chat Assistance. This feature allows you to engage in natural conversations with the improved ChatGPT Assistant right in the TYPO3 backend. Get quick answers, content insights, and support for your daily tasks effortlessly.

**Note: AI may generate inaccurate information about people, places, or facts**

**Step 1** : Navigate to Your T3AI Extension Module and select the Prompt tab.

.. figure:: Images/Chat_assistant_1.png
   :alt: Manage glossary


**Step 2** : Open the 'Chat Assistance' module.

- Select your preferred AI model.
- Enter your prompt. I.e  What is TYPO3?
- Click the 'Go' button.

.. figure:: Images/Chat_assistant_2.png
   :alt: Manage glossary


Helpful Prompts
~~~~~~~~~~~~~~~

No more manual prompt writing or searching. T3AI saves you time and effort with access to hundreds of pre-added AI prompts, tested by expert prompt engineers across various models. Simply select your preferred prompt, customize it, and execute it effortlessly.

**Note - T3AI allows you to customize and create your prompts directly from your backend.**

**Step 1** : Navigate to your T3AI Extension Module and select the 'Prompt' tab. Open the **'Helpful Prompts'** module

**Step 2** : Choose a prompt from the provided list.

.. figure:: Images/Helpful_prompt_1.png
   :alt: Manage glossary


**Step 3** : Select a prompt and add the relevant information.

- Choose the AI model type.
- Enter your query, like 'TYPO3 Developer Agency.'
- Click 'Next' to get your result.

.. figure:: Images/Helpful_prompt_2.png
   :alt: Manage glossary


**Step 4** : Customize your prompt from the provided list and add any additional details.

.. figure:: Images/Helpful_prompt_3.png
   :alt: Manage glossary

For example, you can edit a long-tail keyword prompt. You might have a pre-added prompt like 'List down all the long-tail queries related to [keyword].' You can edit it to say, 'List down all the long-tail queries related to [keyword] with [Intent] and [Numbers].

.. figure:: Images/Helpful_prompt_4.png
   :alt: Manage glossary

After Prompt Edit & Save  - 

.. figure:: Images/Helpful_prompt_5.png
   :alt: Manage glossary

Additionally, you can select and filter prompts using advanced search filters.

.. figure:: Images/Helpful_prompt_6.png
   :alt: Manage glossary


Helpful Prompts Manage
~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kdhzfz01o9e4yti57dyjpk  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Keep your AI prompts organized with the Helpful Prompts Manager. Simplify your tasks and work more efficiently.

**Step 1:** Navigate to your T3AI Extension Module and select the 'Prompt' tab. Open the 'Helpful Prompts Manager' module.

.. figure:: Images/Helpful_Prompt_manage.png
   :alt: Manage glossary

**Step 2:** Choose a prompt from the provided list.

- You can also search by **'Prompt Title,'** **'Prompt Text,'** or **'Type of Prompts.'**
- Select any prompt type from the prompt manager.
- Edit and save the prompt to fit your needs.
- You can also create a new prompt by clicking on the **'Create New Prompt' button.**

.. figure:: Images/Edit_prompt.png
   :alt: Manage glossary

**Step 3:** Edit Prompt from Prompt Manager.

.. figure:: Images/Edit_prompt1.png
   :alt: Manage glossary


**Step 4:** Generate or Create a New prompt.

.. figure:: Images/New_Prompt.png
   :alt: Manage glossary

**Write your own prompt.**

.. figure:: Images/New_Prompt1.png
   :alt: Manage glossary

**Your Generated Prompt listed here -**

.. figure:: Images/New_Prompt2.png
   :alt: Manage glossary

**Result for Prompt**

.. figure:: Images/New_Prompt3.png
   :alt: Manage glossary

Sidebar AI Assistant
~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kfcgw3024ke4ytces5qaaz  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Manage all your work in one place without switching between multiple tabs. T3AI Sidebar AI can handle everything with just a few clicks. Simply click on the 'Sidebar' icon or open the sidebar module. Simplify your workflow with features like summarization, creation, and text rewriting.

.. figure:: Images/sidebar.png
   :alt: Manage glossary

**Step 1:** Open T3AI Sidebar AI and select the prompt type.

- Choose your preferred AI model from the Sidebar AI.
- Edit and save your prompt.
- Add your content in the text box.
- Click the 'Generate AI' button

.. figure:: Images/sidebar1.png
   :alt: Manage glossary

**Types of prompts T3AI supports.**

- Summarize: Quickly create a concise summary of your text.
- Fix Grammar: Correct grammar mistakes in your content.
- Bulletize: Turn your text into easy-to-read bullet points.
- Elaborate: Expand your ideas with more detail and clarity.
- Rewrite: Rephrase your text to improve its wording.
- Custom: Create and customize your own unique prompts.

**Step 2:** Copy your AI response from Sidebar AI and simply paste it into your TYPO3 page.

.. figure:: Images/sidebar2.png
   :alt: Manage glossary

Sidebar AI Prompts
~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kfvxqd02ite4yt9ilsmd4y  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>
Customize your Sidebar AI prompts if you can't find what you're looking for. The Sidebar AI Prompt Manager lets you create, edit, and add your own prompts.

**Step 1:** Open the Sidebar AI Prompts Manager.

- Search for the prompt by title.
- Select the prompt you want to customize and click 'Edit Prompt.'

.. figure:: Images/sidebar3.png
   :alt: Manage glossary

**Step 2:** Customize your AI prompt and rewrite it as needed.

.. figure:: Images/sidebar4.png
   :alt: Manage glossary

**Step 3:** Need to create a custom prompt? Add your own AI prompts.

- Click on the 'Create New Prompt' button.
- Add a prompt title and enter your prompt in the text box.

.. figure:: Images/sidebar5.png
   :alt: Manage glossary

.. figure:: Images/sidebar6.png
   :alt: Manage glossary

.. figure:: Images/sidebar7.png
   :alt: Manage glossary

SEO Prompts
~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kk46dc0540e4ytpibffkiw?step=3  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Keep your AI SEO prompts organized and easily accessible with our Prompts Manager. Manage all your ideas efficiently in one convenient location.

**Step 1** : Open the Sidebar SEO Prompts Manager.

- Search for the prompt by title, text, prompt type, and scope of value.
- Select the prompt you want to customize and click **'Edit Prompt’.**

.. figure:: Images/seo_propmpt_1.png
   :alt: Manage glossary


**Step 2** : Customise your AI prompt and rewrite it as needed.

- You can choose the type of prompt and select the scope, like Title or Topic Outline.
- Edit the prompt title and text.
- Use the default option to set it as the primary prompt for your module.

.. figure:: Images/seo_propmpt_2.png
   :alt: Manage glossary


**Step 3** : Run your prompt on the specific page to check its performance.


Pages Prompts Manager
~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kk46dc0540e4ytpibffkiw?step=18  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Make organising and managing AI Page prompts easy with the Prompts Manager. Keep all your creative ideas in one convenient and accessible place.

**Step 1** : Navigate to the Dashboard, then go to the Prompt Manager.

- Open the 'Page Prompt' module.

.. figure:: Images/prompt_manage_1.png
   :alt: Manage glossary


**Step 2** : Search for the prompt by title, text, or type.

- Select the prompt you want to edit.
- Click the 'Edit Prompt' button.

.. figure:: Images/prompt_manage_2.png
   :alt: Manage glossary


**Step 3** : Customize your Prompts and save the prompt at Prompt Manager.

- You can choose the type of prompt and select the scope, like **Title** or **Topic Outline.**
- Edit the prompt title and text.
- Use the default option to set it as the primary prompt for your module

.. figure:: Images/prompt_manage_3.png
   :alt: Manage glossary


Content Prompts
~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kk46dc0540e4ytpibffkiw?step=28 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Manage your AI content efficiently by organising AI content prompts with the Prompts Manager. Keep all your creative ideas in one place, always accessible and ready to use.

**Step 1** : Navigate to the Dashboard, then go to the Prompt Manager.

- Open the 'Content Prompts' module.

.. figure:: Images/content_prompt_1.png
   :alt: Manage glossary


**Step 2** : Search for the prompt by title, text, or type.

- Select the prompt you want to edit.
- Click the 'Edit Prompt' button.

.. figure:: Images/content_prompt_2.png
   :alt: Manage glossary


**Step 3** : Customise your Prompts and save the prompt at Prompt Manager.

- You can choose the type of prompt and select the scope, like **Title** or **Topic Outline.**
- Edit the prompt title and text.
- Use the default option to set it as the primary prompt for your module.

.. figure:: Images/content_prompt_3.png
   :alt: Manage glossary


**Step 4** : Make Your Own Content AI Prompts.

- Choose the type of prompt and its scope.
- Enter a title for your prompt and write your custom prompt.

.. figure:: Images/content_prompt_4.png
   :alt: Manage glossary


**For Example -**

Write a prompt like: 'Generate [X] word content for my blog post. The writing style should be [Style], with an outline of the blog and in simple English.'

- After writing your prompt, paste it into the Prompt Text box and click the 'Save' button to create your prompt.
- Next, go to the Prompt Manager, select your prompt, run it for a test, and see the results.


Translation Prompts
~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kk46dc0540e4ytpibffkiw?step=40 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Manage all your daily translation tasks in one place. With the T3AI Translation Prompt Manager, you can easily organise and handle all your AI translation prompts. Keep your ideas in one convenient spot, ready to use whenever you need them.

**Step 1** : Navigate to the Dashboard, then go to the Prompt Manager.Open the 'Translation Prompts' module.

.. figure:: Images/trans_prompt_1.png
   :alt: Manage glossary


**Step 2** : Search for the prompt by title, text, or type.

- Select the prompt you want to edit.
- Click the 'Edit Prompt' button.

.. figure:: Images/trans_prompt_2.png
   :alt: Manage glossary


**Step 3** : Customise your Prompts and save the prompt at Prompt Manager.

- You can choose the type of prompt and select the scope, like **Title** or **Topic Outline.**
- Edit the prompt title and text.
- Use the default option to set it as the primary prompt for your module.

.. figure:: Images/trans_prompt_3.png
   :alt: Manage glossary


**Step 4** : Make Your Own Translation AI Prompts.

- Click on Create New AI Prompt.
- Choose the type of prompt and its scope.
- Enter a title for your prompt and write your custom prompt.

.. figure:: Images/trans_prompt_4.png
   :alt: Manage glossary


**For Example -**

Write a prompt like: Translate this content [content] for my blog post. Generate text based solely on the provided prompt without incorporating any additional sense or terms beyond what is explicitly stated. 

- After writing your prompt, paste it into the Prompt Text box and click the 'Save' button to create your prompt.
- Next, go to the Prompt Manager, select your prompt, run it for a test, and see the results.


Media Prompts
~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kk46dc0540e4ytpibffkiw?step=48 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Effortlessly organise and manage AI Media prompts with the Prompts Manager. Keep all your creative ideas together in one easily accessible and ready-to-use location.

**Step 1** : Navigate to the Dashboard, then go to the Prompt Manager.

- Open the 'Media Prompts' module.

.. figure:: Images/media_prompt_1.png
   :alt: Manage glossary


**Step 2** : Navigate to media prompts and open Media prompts.

.. figure:: Images/media_prompt_2.png
   :alt: Manage glossary


**Step 3** : Customise your Prompts and save the prompt at Prompt Manager.

- You can choose the type of prompt and select the scope, like AI Images .
- Edit the prompt title and text.
- Use the default option to set it as the primary prompt for your module.

.. figure:: Images/media_prompt_3.png
   :alt: Manage glossary


**Step 4** : Make Your Own Media Prompts AI Prompts.

- Click on Create New AI Prompt.
- Choose the type of prompt and its scope.
- Enter a title for your prompt and write your custom prompt.

.. figure:: Images/media_prompt_4.png
   :alt: Manage glossary
