.. include:: Includes.txt

Personalise AI user Setting
--------------------------------

T3AI gives you full control over how the AI works. You can easily customize the AI model to match your needs, whether it's for creating content, translations, or improving your website. You can set a default AI model and adjust the settings to make your work faster and more efficient.

- **Step 1** - Log in to your TYPO3 backend and go to your user profile.

Click on the 'User Settings' module."

.. figure:: Images/User_setting.png
   :alt: User_setting

.. figure:: Images/User_setting_1.png
   :alt: User_setting

- **Step 2** - After clicking on **'User Settings,'** navigate to tab **'AI Model Configurations.'**

   - Choose the type of AI configuration you want.

.. figure:: Images/User_setting_2.png
   :alt: User_setting

Now you can use this extension as per your requirement!