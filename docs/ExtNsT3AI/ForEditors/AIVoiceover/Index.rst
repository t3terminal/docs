.. include:: Includes.txt

===============
T3AI Voiceover
===============

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm2vytxhf24kjesgvp4s4kezu?embed_v=2" loading="lazy" title="AI voice over" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

We’re taking big steps to enhance TYPO3 usability! T3AI is now more accessible than ever with our new Voice Accessibility feature, allowing users to create AI-powered voiceovers for any page on their site. With just a few simple steps, you can generate AI-driven voice for your content, enabling visitors to listen, engage, and interact more deeply with your website.

- **Step 1** - Go to your TYPO3 backend and navigate to the page tree. Select the required page, blog, or news page where you’d like to generate an AI voiceover.

For TYPO3 AI Page 
~~~~~~~~~~~~~~~~~~

- Select Page From Page Tree 

.. figure:: Images/Voiceover_1.png
   :alt: T3AI view Statistics

- **Step 2** - Edit your TYPO3 Page Properties and Find AI voicer feature for Generate AI Voice.

   - Click on AI voice over with T3AI 

.. figure:: Images/Voiceover_2.png
   :alt: T3AI view Statistics

- **Step 3** - Click on “AI Voice Over” and navigate to the tab.

    - Select your AI voiceover model, e.g., OpenAI.

    - Choose the model variant and adjust voice speed for optimal usability.

    - Select the tone and type of voice.

    - Choose your preferred output format for the AI voice. 

Click on Generat AI Audio button.

.. figure:: Images/Voiceover_3.png
   :alt: T3AI view Statistics

- **Step 4** - Your AI-powered voice is now generated in your page properties. You can also add it from the storage folder if needed.

.. figure:: Images/Voiceover_4.png
   :alt: T3AI view Statistics

- **Step 5** - Now Go to your TYPO3 Element list and Add T3AI Voice over Plugin in Page to render in Frontend.

.. figure:: Images/Voiceover_5.png
   :alt: T3AI view Statistics

- **Step 6** - Edit your TYPO3 AI Voice Accessibility feature and customize it to fit your needs!

.. figure:: Images/Voiceover_6.png
   :alt: T3AI view Statistics

- **Step 7** - After clicking on Save Changes, view your TYPO3 AI voiceover on the frontend. Your AI-powered voice is now ready!

AI voice over for Blog
~~~~~~~~~~~~~~~~~~~~~~~

With T3AI’s new Voice Accessibility feature, it’s easier than ever to engage with your TYPO3 content! Now, users can listen to your blogs and pages through AI-powered voice recognition, making interactions smoother and more natural.

Turn your content into audio with just one click, making your site more accessible and user-friendly.

- **Step 1** - Follow the same steps as above for TYPO3 Page Voice Over. Go to your blog pages and edit the page properties.

.. figure:: Images/news_voiceover.png
   :alt: T3AI view Statistics

- **Step 2** - Click on “AI Voice Over” and navigate to the tab.

    - Select your AI voiceover model, e.g., OpenAI.

    - Choose the model variant and adjust voice speed for optimal usability.

    - Select the tone and type of voice.

    - Choose your preferred output format for the AI voice.

Click on Generat AI Audio button.

.. figure:: Images/news_voiceover1.png
   :alt: T3AI view Statistics

- **Step 3** - Your AI-powered voice is now generated in your page properties. You can also add it from the storage folder if needed.

.. figure:: Images/news_voiceover2.png
   :alt: T3AI view Statistics

- **Step 4** - Now Go to your TYPO3 Element list and Add T3AI Voice over Element.

.. figure:: Images/news_voiceover3.png
   :alt: T3AI view Statistics

- **Step 5** - Edit your TYPO3 AI Voice Accessibility feature and customize it to fit your needs!

.. figure:: Images/news_voiceover4.png
   :alt: T3AI view Statistics

- **Step 6** - After clicking on Save Changes, view your TYPO3 AI voiceover on the frontend. Your AI-powered voice is now ready!


AI Voice over for news
~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm3fim59v3cg38hspua998evc?embed_v=2" loading="lazy" title="T3AI voiceover News" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Introducing T3AI’s new Voice Accessibility feature—making your TYPO3 news content more engaging and accessible than ever! With AI-powered voice recognition, users can now listen to blogs, articles, and news updates in a smooth, natural-sounding voice, enhancing accessibility and reaching a wider audience.

AI voice over for News (Simple)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note: Please select the Storage folder from the page tree. We recommend using GPT-4.0 for better results.**

- **Step 1** - Navigate to the 'Dashboards' tab, click 'Pages,' and then choose” Create AI News (Simple)”.

.. figure:: Images/News_1.png
   :alt: T3AI view Statistics

- **Step 2** - Open the 'Create AI News (Simple)' tab and generate the News.

- **Step 3** - After News generated,Now go to ns_t3ai tab and generate voice overs.

  - Click on Voice overoption
  - Select your AI voiceover model, e.g., OpenAI.
  - Choose the model variant and adjust voice speed for optimal usability.
  - Select the tone and type of voice.
  - Choose your preferred output format for the AI voice.
  - Click on Generat AI Audio button.

.. figure:: Images/News_2.png
   :alt: T3AI view Statistics

- **Step 3** - Your AI-powered voice is now generated in your page properties. You can also add it from the storage folder if needed.

.. figure:: Images/News_3.png
   :alt: T3AI view Statistics

AI voice over for News (Advance)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Notes:For rendering voice over of news in FE,Ensure You configured the news detail page id in Settings > ns_t3ai > AI audio > News Details Page Id for Voiceover Generate**

.. figure:: Images/News_8.png
   :alt: T3AI view Statistics

**Add AI voice over plugin to your News Detail page for Rendering Voice in Front end**

.. figure:: Images/News_10.png
   :alt: T3AI view Statistics

- **Step 1** - Navigate to the 'Dashboards' tab, click 'Pages,' and then choose” Create AI News (simple)/ News (Advanced)”.

.. figure:: Images/News_4.png
   :alt: T3AI view Statistics

- **Step 2** - Open the 'Create AI News (Advanced)' tab

.. figure:: Images/News_5.png
   :alt: T3AI view Statistics

- Choose the AI model and its versions.
- Select a prompt and customize it if needed.
- Add your "Topic," and "Keywords," and choose the style along with the number of results.
- Click the "Generate AI" button.

- **Step 3:**

 - Select the content outline.
 - Click the "Generate Content" button to create the news content.
 - Click the "Save News Records" button.
 - Also important to click Genertae AI news voice over option while generating news.

**For RTE** - You can see the option for Voice generation while creating news

.. figure:: Images/News_6.png
   :alt: T3AI view Statistics

**For Content Element** - You can see the option for Voice generation while creating News

.. figure:: Images/News_7.png
   :alt: T3AI view Statistics

Now You Can See your generated voice in Tab **Ns T3Ai**

.. figure:: Images/News_9.png
   :alt: T3AI view Statistics

Now You can Generate Voiceover as per your Requirement!

AI Voice over can also be generated with Rewriter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Follow Below Steps to use it,

Go to Pages > Select Rewriter option for Content/Blog/News

.. figure:: Images/Rewriter_1.png
   :alt: T3AI view Statistics

**For Content rewriter**

.. figure:: Images/Rewriter_2.png
   :alt: T3AI view Statistics

**For Blog content rewriter**

.. figure:: Images/Rewriter_3.png
   :alt: T3AI view Statistics

**For News Content rewriter**

.. figure:: Images/Rewriter_4.png
   :alt: T3AI view Statistics
