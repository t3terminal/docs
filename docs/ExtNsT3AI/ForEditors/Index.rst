.. include:: Includes.txt

================================
For Editors
================================

.. toctree::
   :glob:

   Dashboard/Index
   SEO/Index
   AIPages/Index
   Content/Index
   Translation/Index
   T3AIMedia/Index
   Prompts/Index
   PersonaliseAIuserSetting/Index
   AIVoiceover/Index
   AISchema/Index
   AISlug/Index
