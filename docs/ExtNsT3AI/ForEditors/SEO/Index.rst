.. include:: Includes.txt

SEO
------

T3AI SEO
~~~~~~~~~

Improve your TYPO3 site's online presence with Smart T3AI SEO Suggestions. Optimize your website for better rankings on Google, saving you time and effort in seconds.

**Step 1**: Go to the T3AI module, Click on the **“Dashboard tab”**.

**Step 2**: Click on the **“SEO module”** Tab.

.. figure:: Images/desbord-3.png
   :alt: SEO module


One-Click SEO Optimization
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1eqda3z1o7iououd0bn0g10 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Tired of manual SEO tasks? It's time to upgrade your SEO techniques! With T3AI's One-Click SEO feature, you can instantly generate all your SEO metadata, like titles, keywords, descriptions, and social media tags, including OG titles, descriptions, and images in Seconds!

**Note: Please select pages from the page tree. We recommend using GPT-4.0 for better results**

**Step 1:** Navigate to the '**Dashboards**' tab, click '**SEO,**' and then choose '**One-Click AI SEO**.

.. figure:: Images/dasebord-4.png
   :alt: SEO module
   

**Step 2:** **Open the 'All-in-One SEO' tab.**

	- Any AI mode you want to use for generating SEO data.
	- Click on '**Select Model Type.**' i.e  **Gpt 4.0**
	- Enter your SEO title, description, and meta keywords.
	- Click on the '**Generate AI**' button.

.. figure:: Images/dasebord-5.png
   :alt: generate AI 


**Writing Styles**-

**Basic :** The term **“Basic “** describes the content without using complex language or flashy words. The focus is on delivering the essential information in a simple, concise manner.

**Catchy :** A "Catchy style" Term of writing is all about grabbing the reader's attention quickly and keeping them engaged throughout the content.

**High Click-through rate:**  A high click-through rate (CTR) meta title grabs attention, is relevant and encourages users to click on your link instead of your competitors' links.

**Step 3:** after the next step click on select meta title and meta description, similarly check in Keyword and OG graphs.

.. figure:: Images/dasebord-6.png
   :alt: generate AI 

	- Edit your SERP results.
	- Click on the "Save & Close" button.
	- Your data is now saved in your page properties.


Optimized SEO Meta Data
~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1hunqn52ybpouou52j98bf4 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Generate SEO metadata for your page by letting AI analyze the content. It will create meta titles, keywords, descriptions, and other essential SEO details in a few clicks!

**Note: Please select pages from the page tree. We recommend using GPT-4.0 for better results**


**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO,'** and then choose **'SEO metadata”**

.. figure:: Images/dasebord-7.png
   :alt: naigation dasebord


**Step 2:** Open the 'All-in-One SEO' tab.

	- Choose any AI mode you want to use for generating SEO data.
	- Click on **'Select Model Type.** ' i.e  **Gpt 4.0**
	- Enter the number of results.
	- Click on the **'Generate AI'** button.

.. figure:: Images/dasebord-8.png
   :alt: generated


**Step 3:** after the next step click on select meta title and meta description, similarly check in Keyword and OG graphs.

.. figure:: Images/dasebord-9.png
   :alt: Rich text editor1

	- Edit your SERP results
	- Click on the "Save & Close" button.
	- Your data is now saved in your page properties.


Create AI SEO
~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1hs3mqk2x56ououzfqp8wef loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Define your SEO strategy and create smarter, AI-driven SEO. Start a new page and set up your SEO by adding titles, meta keywords, descriptions, and OG metadata.

**Note: Please select pages from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO,'** and then choose **“Create AI SEO”**

**Step 2:** Open the 'Create AI SEO' tab.

.. figure:: Images/dasebord-10.png
   :alt: open AI

	- Add keywords, Topic, and number of results
	- Select meta titles, descriptions, Keywords style, and tone.
	- Click on **“Generate AI“.**

**Step 3:** after the next step click on select meta title and meta description, similarly check in Keyword and OG graphs.

.. figure:: Images/dasebord-11.png
   :alt: check in keyword

	- Edit your SERP results
	- Click on the "Save & Close" button.
	- Your data is now saved in your page properties.


Social Media Meta
~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1huu5w02ykrouou8dji8rly loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

This feature ensures your content is optimized for social media by managing OG tags, descriptions, and images to improve how your content is displayed.

**Note: Please select pages from the page tree.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO'** and then choose **“Social Media Meta”.**

.. figure:: Images/dasebord-12.png
   :alt: open AI


**Step 2:** Open the 'Social Media Meta' tab.

	- Choose any AI mode you want to use for generating OG data.
	- Click on **'Select Model Type.**' i.e  **Gpt 4.0**
	- Enter the number of results.
	- Click on the **'Generate AI'** button.

.. figure:: Images/dasebord-13.png
   :alt: social media meta


**Step 3:** after the next step click on Generate & Select SEO OG data..

.. figure:: Images/dasebord-14.png
   :alt: check on generate


	- Edit your SERP results.
	- Click on the "Save & Close" button.
	- Your data is now saved in your page properties.


Preview SERP Snippet
~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1huxxl12ymfououkvntk0s6 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Check how your website appears on Google by previewing the snippet with Advance T3AI SERP Snippet. Adjust the title, description, and content to make it more appealing to visitors.

**Note: Please select pages from the page tree.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO'** and then choose **“Perview SERP Snippet”.**

.. figure:: Images/dasebord-15.png
   :alt: preview SERP Snippet


Analyze AI Content
~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1hvagl52yqoououvqtliroi loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Looking for an efficient way to analyze your TYPO3 content? T3AI simplifies the process with one-click analysis and AI recommendations to boost your global audience.

**Note: Please select pages from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO'** and then choose  **“Content Analysis”.**

**Step 2:** Open the 'Content Analysis' tab.

.. figure:: Images/dasebord-16.png
   :alt: open contnet Analysis


- Choose any AI mode you want to use for generating a Content Report.
- Click on **'Select Model Type.'** i.e  **Gpt 4.0**
- Select the **“Current page”.**
- Click on the **'Generate AI'** button

**Step 3:** After clicking the next step, click on 'Generate Analysis' to get the content report.

.. figure:: Images/dasebord-17.png
   :alt: content report


SEO Page Score
~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1hvmymq2yy3ouou41s6w698 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Get a quick look at your TYPO3 website's SEO score with just one click! You can easily check your SEO score, see how to improve your ranking, and get recommendations.

**Note: Please select pages from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO'** and then choose  **“SEO Page Score”.**

.. figure:: Images/dasebord-18.png
   :alt: open contnet Analysis


**Step 2:** Open the 'SEO Page Score' tab.

.. figure:: Images/dasebord-19.png
   :alt: open SEO Page Score


- Pick the AI mode you want to use for generating the SEO Page Score
- Click on 'Select Model Type,' like GPT-4.0.
- Enter the page URL or choose the page you want to check for SEO score

**Step 3:** Click on  **“Generate SEO Score“** and Get the Content Score of your pages.

.. figure:: Images/dasebord-20.png
   :alt: content Score


Speed Core Web Vital
~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1hvqjwi2z08ououid345k2k loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Want your website to load faster? With T3AI's "Monitor Speed Core Web Vitals," you get smart AI suggestions to improve your website's speed. This tool helps you optimize key performance areas, making your site run smoother and providing a better experience for your visitors.

**Step 1:** Navigate to the **'Dashboards'** tab, click **'SEO'** and then choose  **“ Speed Core Web Vital”**.

.. figure:: Images/dasebord-21.png
   :alt: open contnet Analysis

**Step 2:** Enter Your URL into Box and click on **“Generate Report”**

.. figure:: Images/Pagespeed.png
   :alt: open contnet Analysis

