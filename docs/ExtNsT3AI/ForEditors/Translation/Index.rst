.. include:: Includes.txt

Translation
-----------

The T3AI Translation feature is a powerful tool designed to simplify content translation. With this feature, you can automatically translate your website's content into multiple languages with just a few clicks. 

.. note:: We do not support translating Flexform-based content elements. Translation is only supported for TCA columns, where fields with l10n_mode set to prefixLangTitle are detected as translatable. This is handled via a DataHandler hook, which only applies to TCA records, not to Flexforms fields.

Manage & Translate Language Glossary
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1iymatz37g5ououpsep6pj8 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


**Step 1**- Navigate to the 'Dashboards' tab, click 'Pages,' and then choose ” Manage glossary”.

.. figure:: Images/trans-1.png
   :alt: Manage glossary


**Step 2** -  Go to the List Module in the TYPO3 Backend.

- Select any page and click on **"New Records."**
- Navigate to the **"Create Glossary"** module and select the glossary.
- Add your term in the text field (e.g., **"TYPO3 Developer"**) and the term you want to replace it with (e.g., **"TYPO3 Agency"**).

.. figure:: Images/trans-2.png
   :alt: Manage glossary


.. figure:: Images/trans-3.png
   :alt: Manage glossary


.. figure:: Images/trans-4.png
   :alt: Manage glossary


**Step 3:** Go to Your TYPO3 Page, Then Translate Your Content element.

- Go to your TYPO3 page and select the content element you want to translate.
- Click on **"Translate with AI."**
- Choose your preferred AI model for the translation. 
- Select the elements you want to translate
- Click the **"Translate"** button.

.. figure:: Images/trans-5.png
   :alt: Manage glossary


.. figure:: Images/trans-6.png
   :alt: Manage glossary


.. figure:: Images/trans-7.png
   :alt: Manage glossary


Re-Translate Pages
~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1iyxwth37itououeb054igl loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Forget about tedious manual translations. With one click, you can refresh and redo your translations instantly. No more manual translation is needed! Just click the 'Re-Translate' button. It will delete the current translations and start the language wizard to begin the translation process.


**Step 1**: Go to the **"Page"** tab and click on **"Pages."**

- Click the **"Re-Translate Page"** button.
- Choose your preferred AI model for translation. i.e **Translate with ChatGPT4.0**
- Select the elements you want to translate.
- Click the **"Translate"** button.

.. figure:: Images/trans-8.png
   :alt: Manage glossary


.. figure:: Images/trans-9.png
   :alt: Manage glossary


.. figure:: Images/trans-10.png
   :alt: Manage glossary


**Step 2**: Click on **"Next."**

- Your content will be re-translated!

.. figure:: Images/trans-11.png
   :alt: Manage glossary


**Step 3**: You can select and edit content directly from your page.

- Click on **"Edit Content."**
- If you want to re-translate the page, select the AI model from the top bar.

.. figure:: Images/trans-12.png
   :alt: Manage glossary


.. figure:: Images/trans-13.png
   :alt: Manage glossary


**Step 4**: Click on Translate & Save the Content! 


News Translation
~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1iz761d37rkououqrjnxb20 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>



Expand your knowledge base with News Translations. Click 'Translate with T3AI' to get translated news directly in your backend.

**Step 1** - Go to the List module and choose the storage folder from the News list. Then, select the news item you want.

.. figure:: Images/trans-14.png
   :alt: T3AI


**Step 2** - **Localize Your Page in Your Preferred Language**

- Open the Page Module and select your page.
- Click the "Translate with T3AI" button.
- Choose your AI model and click "Translate."

.. figure:: Images/trans-15.png
   :alt: Manage glossary


.. figure:: Images/trans-16.png
   :alt: Manage glossary


**Step 3** - Your News is Translated with T3AI.


Records TCA Translation
~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1izdbbg3803ouou220siizb  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Looking for a quick solution to transform your data and language? This T3AI feature lets you easily translate your records and data into different languages using a user-friendly tool.

**Note: We recommend using GPT-4.0 for better results.**

**Step 1** : Open your TYPO3 backend and go to the List module.

**Step 2** : Select the page from the page tree.

**Step 3** : Click on **"Translate with T3AI."**

- Choose the model for translating your TYPO3 TCA records.
- Click the "Translate" button.

.. figure:: Images/trans-17.png
   :alt: Manage glossary


.. figure:: Images/trans-18.png
   :alt: Manage glossary


**Step 4** : Click on the "Translated Page."

- Edit the translated page.
- On the top bar, click the "Translate with T3AI" button.
- Click the "Translate" button.
- Save your translated records.

.. figure:: Images/trans-19.png
   :alt: Manage glossary


.. figure:: Images/trans-20.png
   :alt: Manage glossary


.. figure:: Images/trans-21.png
   :alt: Manage glossary


Activate Translated Content
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1izi7f2385jououzft4el5g  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Tired of spending time manually enabling or disabling translated content on your TYPO3 pages? T3AI makes it easy and quick! By default, translated content is turned off, but with just one click on the "Activate Translated Content" button, you can instantly enable it. Save time and let T3AI handle the work for you!


**Step 1**: Open your TYPO3 backend.

**Step 2**: Go to the Page module and select a page from your page tree.

**Step 3**: On your TYPO3 page, choose the language layout.

**Step 4**: Translate your page with T3AI.

**Step 5**: Click the "Translate with T3AI" button.

.. figure:: Images/trans-22.png
   :alt: Manage glossary


**Step  6**: Once the page is translated, simply click the "Activate Translated Content" button.

With one click, all your disabled content and elements will be activated.


.. figure:: Images/trans-23.png
   :alt: Manage glossary


.. figure:: Images/trans-24.png
   :alt: Manage glossary


Extensions Localization (XLF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j05tca38eiououc88mc21l  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Struggling with language issues in your TYPO3 product? T3AI is here to help. Expand your product globally without language barriers by using our easy-to-use T3AI  XLF translations. Transform your TYPO3 extensions into multilingual ones in just a few clicks!

**Step  1** : Navigate to the T3AI module, Click on “ Translation Tab”.

**Step  2** : Open The  “ Extensions Localization (XLF) “  Module.

.. figure:: Images/trans-25.png
   :alt: Manage glossary


**Step 3** : Select your TYPO3 extension from your list. You can also search by text or filter files directly from the module using the additional features.

.. figure:: Images/trans-26.png
   :alt: Manage glossary


**Step 4** : Click on the "New Localization" button to translate your extension file into another language.

- Navigate to "Installed Extensions" and select your extension.
- Choose the default language file name.
- Select the target language, e.g., German.
- Next, choose the "Translation Mode" type. You have two options:
   - **Translate the entire file and overwrite any existing translations**: This means T3AI will completely translate your extension files from scratch.
   - **Translate only the missing items**: T3AI will only translate terms or lines that are missing or have not been translated yet.
- Click on the “ Translate File “ button.

.. figure:: Images/trans-27.png
   :alt: Manage glossary


**Step 5**: Your translated file has been created. You can now edit and save it. Your XLF file is ready!

.. figure:: Images/trans-28.png
   :alt: Manage glossary


.. figure:: Images/trans-29.png
   :alt: Manage glossary


New Localization (XLF)
~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j0ig3w38keououu46vdth1?step=3  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


If you want to create a new XLF file version, just follow the same steps as above. This will generate your new XLF file localization.


`Start AI Localizing <https://docs.t3planet.com/en/latest/ExtNsT3AI/HowToUse/ForEditors/Index.html#extensions-localization-xlf>`_


Translate Content Elements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With this T3AI feature, you can easily translate your page content elements with just one click! It simplifies the translation process, saving you time and effort. No more manual copying and pasting—just select the content, choose your language, and let T3AI handle the rest

- **Step 1** - Go to your TYPO3 dashboard and select the **'Page'** module.

  - Choose the page from your page tree and navigate to it.

.. figure:: Images/Element_translate.png
   :alt: Manage glossary

- **Step 2** - Edit Your Content Element and Select Transalte with the T3AI button.

.. figure:: Images/Element_translate_1.png
   :alt: Manage glossary

- **Step 3** - Select your AI model and language.

   - Click on the **'Translate Record'** button.
   - Your content element will be translated

.. figure:: Images/Element_translate_2.png
   :alt: Manage glossary

.. figure:: Images/Element_translate_3.png
   :alt: Manage glossary

