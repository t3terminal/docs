.. include:: Includes.txt

T3AI Media
----------

Make AI-generated images, audio, and videos quickly and easily. Use trusted tools like DALL-E, MidJourney, Stability AI, Unsplash, Openverse, and Pixabay to create your media.


DALL-E AI Images
~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1glccfn2ho8ououqex9x5sj  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Your T3AI's image generation capabilities are now even stronger with DALL-E integration. Create high-quality AI-generated images with just one click!

**Note: Before Starting AI image generation, you need to select folders from Your folder tree.**

**Step 1** : Navigate to Your T3AI TYPO3 KI Extension Module and select the Media tab.

**Step 2** : Open the “ Dall-E AI images“ module. 

.. figure:: Images/media-1.png
   :alt: Manage glossary


**Step  3** : Enter the AI image prompt describing the image you want to create.

- Choose the image style, like realistic or sketch.
- Select the image size that fits your needs.
- Enter the number of image results you want to generate.
- Click the "Generate AI Image" button.

.. figure:: Images/media-2.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

.. figure:: Images/media-3.png
   :alt: Manage glossary


**Step 5** : Click the "Save Image" button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-4.png
   :alt: Manage glossary



MidJourney AI Images
~~~~~~~~~~~~~~~~~~~~
Create any image you can imagine in seconds! With T3AI’s powerful integration with Midjourney AI, you can create high-quality images from simple text prompts for free. Just type a description of the image you want, and you'll get amazing results.


**Note: Before Starting AI image generation, you need to select folders from Your folder tree.**

**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the **“MidJourney AI Images“** module. 

**Step 3** : Enter the AI image prompt describing the image you want to create.
 
- Click the **"Generate AI Image"** button.

.. figure:: Images/media-5.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

.. figure:: Images/media-6.png
   :alt: Manage glossary


**Step 5** : Click the "Save Image" button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-7.png
   :alt: Manage glossary


Stability AI Images
~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j7zy243czyououmpakohlr  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Create cutting-edge AI images and videos with advanced AI technology. Stability AI media can do it with one click!

**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the “ Stability AI Images“ module.

.. figure:: Images/media-8.png
   :alt: Manage glossary


**Step 3** : Enter the AI image prompt describing the image you want to create.

   - Click the **"Generate AI Image"** button.

.. figure:: Images/media-9.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

**Step 5** : Click the "Save Image" button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-10.png
   :alt: Manage glossary


Unsplash AI
~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j644sm3bhgouou404w6ymd  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Looking for a collection of stock images? Explore a diverse selection of AI-generated images, perfect for any project or theme. Easily integrated into your work, these images are ready to enhance your creativity.

**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the “Unsplash AI“ module.

.. figure:: Images/media-11.png
   :alt: Manage glossary


**Step 3** : Enter the AI image prompt describing the image you want to create.

- Click the **"Generate AI Image"** button.

.. figure:: Images/media-12.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

.. figure:: Images/media-13.png
   :alt: Manage glossary


**Step 5** : Click the **"Save Image"** button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-14.png
   :alt: Manage glossary


Openverse AI
~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j6bcpw3bloououulu65jni  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Discover a vast collection of freely licensed media. Easily search for and use images, audio, and videos from Openverse for all your creative projects. Whether you're working on a website, presentation, or any other project, Openverse provides high-quality resources that are easy to integrate and use.


**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the **“Openverse AI“* module.

.. figure:: Images/media-15.png
   :alt: Manage glossary


**Step 3** : Enter the AI image prompt describing the image you want to create.

- Add Proper Image size & Number of images you want to create.
- Click the **"Generate AI Image"** button.

.. figure:: Images/media-16.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

.. figure:: Images/media-17.png
   :alt: Manage glossary


**Step 5** : Click the **"Save Image"** button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-18.png
   :alt: Manage glossary
   

Pixabay AI
~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j6fq743bqpououebq0gl85  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Choose from a wide variety of images and videos to fit any project with Pixabay integration. Whether you're designing a website, creating a presentation, or working on a marketing campaign, Pixabay offers a rich collection of high-quality media that’s easy to access and use in your projects.

**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the **“Pixabay AI“** module

.. figure:: Images/media-19.png
   :alt: Manage glossary


**Step 3** : Enter the AI image prompt describing the image you want to create.

- Add Proper Image size & Number of images you want to create.
- Select the Number of images you want to create.

.. figure:: Images/media-20.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

.. figure:: Images/media-21.png
   :alt: Manage glossary


**Step 5** : Click the **"Save Image"** button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-22.png
   :alt: Manage glossary


Pexels AI
~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j77jn93cjtouou6inkqf6t  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Generate and use high-quality, free stock photos from Pexels to easily enhance your content. Whether you're working on a blog, social media post, or any other project, Pexels provides a wide selection of beautiful images that can be seamlessly integrated to make your content stand out.

**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the **“Pexels AI“** module.

.. figure:: Images/media-23.png
   :alt: Manage glossary


**Step 3** : Enter the AI image prompt describing the image you want to create.

- Add Proper Image size & Number of images you want to create.
- Select the Number of images you want to create.

.. figure:: Images/media-24.png
   :alt: Manage glossary


**Step 4** : Select the image you want to save. Additionally, you can sort the images in either row or list format.

.. figure:: Images/media-25.png
   :alt: Manage glossary


**Step 5** : Click the **"Save Image"** button, and your AI-generated image will be saved in the appropriate folder.

.. figure:: Images/media-26.png
   :alt: Manage glossary


AI Audio
~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j6lzy53bzcououdsr6lhhk  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j6rnqm3c50ouougmr9cpg5 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Looking for an efficient way to create AI audio for your presentations and videos? T3AI is the solution. Just input your script or text, and produce high-quality AI audio in no time with our powerful features.

**Step 1** : Navigate to Your T3AI Extension Module and select the Media tab.

**Step 2** : Open the “AI Audio“ module.

.. figure:: Images/media-27.png
   :alt: Manage glossary


**Step 3** : Select Your Preferred AI Model for AI Audio Generation.

- Choose the AI model variant.
- Pick different voice types, like Alloly.
- Adjust the audio speed and select the output file format.
- Choose a folder from your folder tree.
- Click the “Generate AI Audio” button.

.. figure:: Images/media-28.png
   :alt: Manage glossary


**Step 4** : Navigate to your selected folder, select the generated AI audio file, and click on the download option.

.. figure:: Images/media-29.png
   :alt: Manage glossary


AI Filemeta 
~~~~~~~~~~~~~

The AI File Meta feature in T3AI helps you create metadata for your images easily.

- Automatically generate metadata for individual images, including titles, descriptions, and alt text.

- Automatically generate metadata in bulk for multiple files.

File Meta (TextAlt.ai API)
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm42eem100w9010xuc0keoz39?embed_v=2" loading="lazy" title="File Alt Text(AltText.ai)(German)" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

With Alt AI API, streamline your metadata creation process like never before. Generate alt text for images, videos, and other files in just seconds. This powerful tool ensures your content is well-organized, accessible, and ready to add Alt text to your images with ease.

**Steps to Generate AI Alt Metadata:**

**Step 1** : Navigate to the File List in your TYPO3 backend.  

**Step 2** : Choose a folder from the Folder List.  

**Step 3** : Open the Image List and select an image.  

**Step 4** : Click on **Edit Image Alt Metadata**.  

**Step 5** : Click the **Generate AI Alt Metadata** button.  

**Step 6** : Select the **TextAlt.ai API** model.  

**Step 7** : Click the **Generate** button.  

**Step 8** : Once the AI-generated alt text is ready, click **Save**.  

**Step 9** : Your AI-powered alt text is automatically added to the image field.  

File Meta (Vision API)
~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm41crfrl0fwfjfcxqavljnqb?step=1" loading="lazy" title="File Alt Text(AltText.ai)(German)" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

With File Meta (Vision API), automate metadata generation using advanced AI vision capabilities. Extract and tag images, videos, and other files effortlessly—smart, accurate, and fast.

**Steps to Generate AI Alt Metadata:**

**Step 1** : Navigate to the File List in your TYPO3 backend.  

**Step 2** : Choose a folder from the Folder List.  

**Step 3** : Open the Image List and select an image.  

**Step 4** : Click on **Edit Image Alt Metadata**.  

**Step 5** : Click the **Generate AI Alt Metadata** button.  

**Step 6** : Select the **Vision API** model.  

**Step 7** : Click the **Generate** button.  

**Step 8** : Once the AI-generated alt text is ready, click **Save**.  

**Step 9** : Your AI-powered alt text is automatically added to the image field.  

AI Bulk Metadata
~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm41cct570fp5jfcx0hcl33mz loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Managing a large number of image files? No worries! With File Meta Bulk, you can process metadata for multiple files simultaneously. Save time and effort with AI-powered batch processing, making your workflow efficient and hassle-free.

**Steps for AI Bulk Metadata Generation:**

**Step 1** : Navigate to the File List in your TYPO3 backend.  

**Step 2** : Select a folder from the Folder List.  

**Step 3** : Click on the **"Mass AI File Meta"** button.  

**Step 4** : Choose your preferred option:  
  - **Queue this folder & generate only missing metadata**.  
  - **Queue this folder & override all metadata**.  

**Step 5** : Run the scheduler from the **System Module**.  

**Step 6** : Once the scheduler has successfully executed, navigate back to the **File List** module.  

**Step 7** : Select any image from the list—your AI-generated metadata, including alt text, will have been automatically added to the files.
