.. include:: Includes.txt

T3AI copilot
~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1an0qzi0xy3ououtbwj6lmz loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

This feature adds an AI copilot to the TYPO3 Rich Text Editor (RTE), making it easier for users to add and update content. The copilot offers helpful suggestions and simplifies editing and formatting, improving the overall user experience.

Follow below Step to Use this feature,

- **Step 1** - goto page module and Select the page

- **Step 2** - click on edit Page property

- **Step 3** - go to Tab Resources

- **Step 4** - include **Ns T3Ai :: Config RTE Preset (ns_t3ai)** in Include static Page TSconfig and Save the configuration

.. figure:: Images/Page_TSconfig.png
   :alt: Manage glossary

Now add RTE in into page and you can see  AI-copilot in RTE 

.. figure:: Images/AICopilot.png
   :alt: Manage glossary

.. figure:: Images/T3ai_Copilot.png
   :alt: Manage glossary


T3AI Prompts
~~~~~~~~~~~~

This feature helps optimize your inserted content using prompts.

.. figure:: Images/T3AI_Prompts.png
   :alt: Manage glossary

T3AI Transalator
~~~~~~~~~~~~~~~~~

This feature allows you to translate selected content into your chosen language.

.. figure:: Images/T3AI_Translation.png
   :alt: Manage glossary

