.. include:: Includes.txt
   
AI Pages
---------

Launch the TYPO3 AI Page
~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm10p4mfy06f41205pk2yb5u6 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Tired of manually setting up TYPO3 pages? With T3AI TYPO3 Extension, you can launch your page in seconds. Just pick a prompt and enter your keyword, and T3AI will take care of the rest. This tool quickly builds an AI-powered page using your titles, content, metadata, images, and other elements—all with just one click!

**Note: Choose the type of pages you want to work with. For the best results, we recommend using GPT-4.0.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages'** and then choose **” Launch AI Page”.**

.. figure:: Images/dasebord-22.png
   :alt: open Launch AI Page

**Step 2:** Select the type of page you want to create with T3AI. Choose the AI model and version you want to use.Select Prompt

- Enter Keywords 
- Select the number of Results.
- Click on “ Generate AI”

.. figure:: Images/dasebord-23.png
   :alt: create with T3AI


**Step 3:**  Select the page title and click on "Generate Outline."  Check or uncheck the content elements you want to include on the page.

- Choose between two options:
- 1- "Create elements on existing page" - This option lets you add AI-generated content to your current page with just one click. Your page will be updated automatically!
- 2- "Create new subpages" - This option lets you create an AI subpages page with the Current Page.

.. figure:: Images/dasebord-24.png
   :alt: content elements


**Step 4:** Click on **'Create elements on existing page,** ' and your page will be created with content.

.. figure:: Images/dasebord-25.png
   :alt: content elements


Launch Advance AI TYPO3 Page
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm10qytub07a91205d6xwzqcj loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Make editing and integration easier with T3AI’s Advanced AI TYPO3 Page feature. Build AI-powered pages by choosing the type of page you want to create. Select your topics and create an outline, add multiple elements and images—all with just a few clicks!


**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **Launch AI Page”.**

.. figure:: Images/dasebord-26.png
   :alt: open Launch AI Page

**Step 2:** Choose the type of page from the pre-defined categories (Default, Blog, Product, Service).

- Pick the AI model and version you want to use. 
- Select a prompt, add your keywords, and choose the number of results.
- Click on “ Generate AI”.

.. figure:: Images/dasebord-27.png
   :alt: Generate AI


**Step 3:**  Select the page title and click on **"Generate Outline."**  Check or uncheck the content elements you want to include on the page.

- Choose between two options:
- **"Create elements on existing page"** - This option lets you add AI-generated content to your current page with just one click. Your page will be updated automatically!
- **"Create new subpages"** - This option lets you create an AI subpages page with the Current Page

.. figure:: Images/dasebord-28.png
   :alt: Generate Outline


**Step 4:** Your content will automatically added to the Page with the Selection of elements.

.. figure:: Images/dasebord-29.png
   :alt: Selection of elements.


Build AI Page Tree
~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1aduj3b0tvcououxlztj86k loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Create an AI-generated TYPO3 Page Tree with just a single click! This feature automatically builds a complete structure of web pages and menus for your website using AI. It saves you time by organizing your site in a logical, easy-to-navigate manner, ensuring that all necessary pages are created and connected seamlessly.


**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **Build AI Page Tree ”.**

**Step 2:** **Open Module  “Build AI Page Tree ”.**

- Pick the AI model and version you want to use.
- Select a prompt you want to build an AI Page Tree.
- Click on “Generate AI”.

.. figure:: Images/dasebord-30.png
   :alt: Generate AI


**Step 3:**  Select a page from the generated results.

- You can edit or delete any pages as needed.
- To regenerate a page, click on "Regenerate Page."
- Finally, click on "Create AI Page Tree" to complete the process.

.. figure:: Images/dasebord-31.png
   :alt: Create AI Page


**Step 4:** Your TYPO3 page will be generated.

- Go to the "Selected Page."
- Click on "Generated Page Tree" to view it.

.. figure:: Images/dasebord-32.png
   :alt: Generated Page Tree


Build AI Blog Tree
~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm38pbuhy0y5o8hsp1ssz8vqt?embed_v=2" loading="lazy" title="AI Blog tree" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Organize and manage your blog content effortlessly with the AI Blog Tree feature! This tool helps you structure your TYPO3 blog content in a clear, organized tree format, making it easier for users to navigate and explore your posts. 

- **Step 1** - Go to the TYPO3 Page Tree and select the blog root page. 

Then, go to the T3AI dropdown list and select 'AI Page Tree' from the Page module.

.. figure:: Images/blogtree.png
   :alt: Generated Page Tree

.. figure:: Images/blogtree1.png
   :alt: Generated Page Tree

- **Step 2** - Select your preferred AI models and add your AI blog page tree prompt.

- Click on Generate page tree button.

- **Step 3** -Edit your Blog page tree and save necessary changes.

.. figure:: Images/blogtree2.png
   :alt: Generated Page Tree

**Step 4** - Your Ai powerd blog tree is created in seconds !

Create AI News (Simple)
~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm19e83y30iw2ououkpcoziv0 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Are you still writing news the old-fashioned way? Looking for a smarter solution to create news quickly? With T3AI, you can generate compelling news articles in just minutes! This feature allows you to easily produce high-quality news content with just a few steps. Simply add your topics, keywords, and the type of news you want, and T3AI will do the rest.

**Note: Please select the Storage folder from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **Create AI News (Simple)”.**

.. figure:: Images/dasebord-33.png
   :alt: Generate AI


**Step 2:** Open the 'Create AI News (Simple)' tab.

.. figure:: Images/dasebord-34.png
   :alt: Generate AI

- Choose any AI mode you want to use for generating  Create AI News.
- Click on 'Select Model Type.' i.e  Gpt 4.0
- Add “ Topic “ and “ Keywords “.
- Enter the number of results.
- Click on the 'Generate AI' button.


**News writing style -** 

**Formal** - This News style creation ensures well-structured, polished articles that maintain a professional tone and accuracy.

**Informal** - Writing in an informal style feels more like a casual conversation. It's relaxed, friendly, and easy to understand, making it great for connecting with your audience on a personal level.

**Concise** - Concise news writing means expressing your ideas clearly and directly, using as few words as possible.

**Creative** - Creative writing style adds flair and imagination, making your content more engaging and unique


**Step 3:**  Edit Your Content

- Edit the news headline, URL, teaser, and RTE text.
- Edit the keywords and meta description.
- If you need more content ideas, click on **"Get AI Suggestions."**
- Once you're done, click "Close Page." Your news will be saved to your storage!

.. figure:: Images/dasebord-35.png
   :alt: Create AI Page


**Step 4:** Go to the List module.

- Click on your respective storage folder.
- Click and view the generated news.

.. figure:: Images/dasebord-36.png
   :alt: Generated Page Tree


Create AI News (Advanced)
~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/showcase/cm1gm7tws00c214kkyqjyrdhz?demo=13 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Create smarter, AI-driven news with T3AI Advanced AI News. Build a powerful AI-based news platform by selecting topics, outlining stories, and adding multiple elements or images—all in just a few clicks!

**Note: Please select the Storage folder from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **Create AI News (Advanced)”.**

.. figure:: Images/dasebord-37.png
   :alt: Create AI News (Advanced)


**Step 2:** Open the 'Create AI News (Advanced)' tab.

.. figure:: Images/dasebord-38.png
   :alt: Generate AI

- Choose the AI model and its versions.
- Select a prompt and customize it if needed.
- Add your **"Topic,"** and **"Keywords,"** and choose the style along with the number of results.
- Click the **"Generate AI"** button


**Step 3:** 

- Select the content outline
- Click the "Generate Content" button to create the news content.
- Click the "Save News Records" button.

.. figure:: Images/dasebord-39.png
   :alt: Save News Records


**Step 4:** Edit Your Content

- Edit the news headline, URL, teaser, and RTE text.
- Edit the keywords and meta description.
- If you need more content ideas, click on "Get AI Suggestions."
- Once you're done, click "Close Page." Your news will be saved to your storage!

.. figure:: Images/dasebord-40.png
   :alt: Content


**Step 5:** Go to the List module

- 1.Click on your respective storage folder.
- 2.Click and view the generated news.

.. figure:: Images/dasebord-41.png
   :alt: generated news

Blog (Simple)
~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1ut0h2b0e8k13b318c7jom8 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

You can quickly create an engaging blog with AI in a few simple steps. Just pick a topic, include some keywords, and choose the blog style. The AI will do the rest, helping you generate content easily!

Follow below step to generate the Blog

- **Step 1:** Go to Module **"Dashboard"**

- **Step 2:** Go to tab Page

- **Step 3:** Click on button **"Generate AI Blog"**

.. figure:: Images/Blog_simple_1.png
   :alt: generated news

.. figure:: Images/Blog_simple_2.png
   :alt: generated news

Select Model,add topic,generate and add content to your blog page! 

Blog (Advance)
~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1utd6q50eis13b345ixdroi loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

With this feature, you can create an advanced AI-powered blog by selecting topics, outlining, and adding images and elements!

Follow below step to generate the adavanced blog

Follow below step to generate the Blog

- **Step 1:** Go to Module **"Dashboard"**

- **Step 2:** Go to tab Page

- **Step 3:** Click on button **"Generate AI Blog"**

.. figure:: Images/Blog_advance_1.png
   :alt: generated news

.. figure:: Images/Blog_advance_2.png
   :alt: generated news

Select Model,add topic,generate and add content to your blog page! 