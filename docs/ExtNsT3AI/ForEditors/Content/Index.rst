.. include:: Includes.txt

Content
--------

T3AI copilot
~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1an0qzi0xy3ououtbwj6lmz loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

This feature adds an AI copilot to the TYPO3 Rich Text Editor (RTE), making it easier for users to add and update content. The copilot offers helpful suggestions and simplifies editing and formatting, improving the overall user experience.

Follow below Step to Use this feature,

- **Step 1** - goto page module and Select the page

- **Step 2** - click on edit Page property

- **Step 3** - go to Tab Resources

- **Step 4** - include **Ns T3Ai :: Config RTE Preset (ns_t3ai)** in Include static Page TSconfig and Save the configuration

.. figure:: Images/Page_TSconfig.png
   :alt: Manage glossary

Now add RTE in into page and you can see  AI-copilot in RTE 

.. figure:: Images/AICopilot.png
   :alt: Manage glossary

.. figure:: Images/T3ai_Copilot.png
   :alt: Manage glossary


T3AI Prompts
~~~~~~~~~~~~

This feature helps optimize your inserted content using prompts.

.. figure:: Images/T3AI_Prompts.png
   :alt: Manage glossary

T3AI Transalator
~~~~~~~~~~~~~~~~~

This feature allows you to translate selected content into your chosen language.

.. figure:: Images/T3AI_Translation.png
   :alt: Manage glossary


Content Rewriter
~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1c33ti41da3ououg183xhs9 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Stop spending hours rewriting content by hand. T3AI Content Writer lets you generate fresh and unique content quickly and easily. Whether you need blog posts, news articles, or website content, T3AI Content Writer has you covered. Generate tailored content in seconds.

**Note: Please select the Storage folder from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **Content Rewriter”.**

**Step 2:** Open the 'Content Rewriter’' tab.

.. figure:: Images/dasebord-38.png
   :alt: Generate AI

- Choose the AI model and its version.
- Select a prompt and customize it if needed.
- Add your **"Select Record Page,"** **"Topic," "Writing Style," "Word Count," "Additional Instructions,"** and **"Auto Link."**
- Click the **"Generate AI"** button.


**Record type -**

- Blog: Refresh your blog content quickly with just a few clicks.
- News: Update your news articles instantly with our easy-to-use tool


**Writing Style -**

**Casual**  - Keep it light and friendly for a more relaxed feel.

**Approachable** - Make your content welcoming and easy to connect with users.

**Easy readability** - this style Ensures your content is clear and simple for everyone to understand.


**Rewrite in Different Styles -**

**Internal**: Choose a page from the Page Tree and rewrite it in your preferred style.

**External**: Add an external link, fill in the fields, and click "Generate AI."

**Text**: Enter your content in the text box, select the content type, and click "Generate AI."

.. figure:: Images/dasebord-38.png
   :alt: Generate AI


**Step 3:** 

- Select the content outline
- Click the "Generate Content" button to create the news content.
- Click the "Save News Records" button.

.. figure:: Images/dasebord-39.png
   :alt: Save News Records


**Step 4:** Edit Your Content

- Edit the news headline, URL, t easer, and RTE text.
- Edit the keywords and meta description.
- If you need more content ideas, click on "Get AI Suggestions."
- Once you're done, click "Close Page." Your news will be saved to your storage!

.. figure:: Images/dasebord-40.png
   :alt: Content


**Step 5:** Go to the List module

- 1.Click on your respective storage folder.
- 2.Click and view the generated news.

.. figure:: Images/dasebord-41.png
   :alt: generated news



News Content Rewriter
~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1c4dw851dhhouou23yess6j loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


No more manual rewriting! T3AI Content Writer helps you create fresh content quickly and easily. Use our AI tool to generate original articles in seconds. Our online AI tool helps you generate original articles in just a few seconds.

**Note: Please select the Storage folder from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **News Content Rewriter”.**

**Step 2:** Open the 'Content Rewriter’' tab.

.. figure:: Images/dasebord-42.png
   :alt: Generate AI

- Choose the AI model and its version.
- Select a prompt and customize it if needed.
- Add your **"Select Record Page," "Topic," "Writing Style," "Word Count," "Additional Instructions,"** and **"Auto Link."**
- Choose **“Generate News Records”**  from the Storage Folder.
- Click the **"Generate AI"** button.


**Record type -**
- Blog: Refresh your blog content quickly with just a few clicks.
- News: Update your news articles instantly with our easy-to-use tool


**Writing Style -**

**Casual**  - Keep it light and friendly for a more relaxed feel.

**Approachable** - Make your content welcoming and easy to connect with users.

**Easy readability** - this style Ensures your content is clear and simple for everyone to understand.


**Rewrite in Different Styles -**

**Internal**: Choose a page from the Page Tree and rewrite it in your preferred style.

**External**: Add an external link, fill in the fields, and click "Generate AI."

**Text**: Enter your content in the text box, select the content type, and click "Generate AI."

.. figure:: Images/dasebord-43.png
   :alt: Generate AI


**Step 3:** Choose the element type and edit the content.

- Click the "Save" button.
- Your news is now rewritten!

.. figure:: Images/dasebord-44.png
   :alt: Save News Records


**Step 4:** Your Rewritten content is stored in your elements !


.. figure:: Images/dasebord-45.png
   :alt: Content


**Step 5:** 

- Go to the List module 
- Select your storage folder.
- Edit & View your News by Select “ News records”.


.. figure:: Images/dasebord-46.png
   :alt: generated news


Blog Content Rewriter
~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1ix22n736u4ououcxefep9v loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Generate fresh and unique content in seconds with our T3AI rewriting tool.Create original articles and contents with the assistance of our online AI content rewriting tool in just a few seconds.

**Note: Please select the Storage folder from the page tree. We recommend using GPT-4.0 for better results.**

**Step 1:** Navigate to the **'Dashboards'** tab, click **'Pages,'** and then choose” **Blog Content Rewriter”.**

.. figure:: Images/content-1.png
   :alt: Generate AI


**Step 2:** Open the 'Blog Content Rewriter' tab.

- Select the AI model and its version.
- Choose the rewriter type: Internal, External, or Text.
- Enter the blog topic and select the record type.  
- Choose your preferred writing style.
- Set the word count and add any additional instructions, including Auto Link and Text.
- Click the 'Generate AI' button

.. figure:: Images/content-2.png
   :alt: Generate AI


**Types of Blog Rewriter AI -**

**Internal Blog Rewriter** – This T3AI feature lets you select a blog from your list and rewrite it with just one click!

**External Blog Rewriter AI** - Simply provide the URL of an external blog and follow the same steps. T3AI can rewrite the blog in just a few clicks!

**Text** - This option allows you to dynamically add content from an external source and rewrite the blog within minutes!


**Step 3:** Edit and choose the elements you want to rewrite in your blog content.

- T3AI lets you select different element types.
- Edit the content of each element as needed.
- You can also delete and regenerate content if needed. 
- Click the 'Save' button when you're done.

.. figure:: Images/content-3.png
   :alt: Save News Records


**Step 4:** Your Rewritten content is stored in your elements !


.. figure:: Images/content-4.png
   :alt: Content


**Step 5:** 

- Go to the List module  
- Select your storage folder.
- Edit & View your News by Select “ Blog records”.


Content Elements (Create)
~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

  <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1aljgtf0x13ououfoh135fv loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Create AI-powered content elements effortlessly. With advanced AI, you can easily generate TYPO3 core elements, along with custom content elements and blocks that match your design. This feature allows you to quickly build and customize content, saving you time and making your work more efficient. 

**Step 1:** Go to the Page module and select the page where you want to create an element.

**Step 2:** Add content elements by selecting 'Create Element with T3AI' from the top bar.

.. figure:: Images/content-5.png
   :alt: Generate AI


**Step 3:** Create the element using T3AI.

- Enter your content element prompt and choose the text fields (headers, sub-headers, text).
- Add an AI image prompt, choose the AI image settings, and select the AI image models.
- Click the 'Generate AI' button. 

.. figure:: Images/content-6.png
   :alt: Save News Records


**Step 4:** your Regenerate Content is stored in your selected Element filed.

.. figure:: Images/content-7.png
   :alt: Content


Content Elements (Edit)
~~~~~~~~~~~~~~~~~~~~~~~~
Guidance
************
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1ixi1mc372xououq1k4jrfn loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Didn't get the information you wanted? T3AI lets you edit and regenerate elements in seconds, saving you time and effort. Easily edit AI-generated TYPO3 core components, as well as custom content elements and blocks you've created.

**Note - Follow the steps outlined above under -**

.. figure:: Images/content-8.png
   :alt: Generate AI


**Step 1:** Go to the Page module and select the page where you want to create an element.


AI Forms
~~~~~~~~~

Tired of creating forms manually? Meet your new AI-powered form-building assistant! With AI Forms, creating forms is as easy as a single click—smart, fast, and ready to use in seconds.

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/demo/cm41b630q0enajfcxh1nk1wmj loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

- **Step 1:** Log in to your TYPO3 backend.  

- **Step 2:** Navigate to the TYPO3 Form module.  

- **Step 3:** Click on the "Generate AI Form" button.  

- **Step 4:** Choose your AI module (e.g., ChatGPT).  

- **Step 5:** Select the AI model type for optimal results.  

- **Step 6:** Enter your form prompt (e.g., "Contact Form").  

- **Step 7:** Click "Generate AI Form."  

Your AI-Powered Form is Ready in Seconds!

