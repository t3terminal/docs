.. include:: Includes.txt


Dashboard
---------

OpenAI Stats
~~~~~~~~~~~~~~~~

This TYPO3 Extension streamlines your entire TYPO3 editing process, from SEO to managing prompts, all in one place.

Access all your AI model analytics in one place with an All-in-one T3AI dashboard that includes predefined AI model statistics.


**Step 1**: Go to the T3AI module, Click on the “ **Dashboard tab**”.

.. figure:: Images/dasebord-1.png
   :alt: T3AI dasebord


**Step 2**: Go to “ **OpenAI Stasts**”. Click on “ **View Statistics**” and Get a quick summary of your OpenAI API usage, including costs, credits, requests, tokens, and more.

.. figure:: Images/dasebord-2.png
   :alt: T3AI view Statistics

