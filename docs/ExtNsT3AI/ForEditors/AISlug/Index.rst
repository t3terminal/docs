.. include:: Includes.txt

==========
T3AI Slug
==========

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm38qx3qk0zjj8hsp6qqdi2yr?embed_v=2" loading="lazy" title="AI Slug" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Easily create SEO-friendly URLs with the AI Slug feature! T3AI helps you generate clear, optimized slugs for your pages,blogs and news , making your content easier for search engines and users to understand.

- **Step 1** - Move to your TYPO3 Page and select any type of page from list.

.. figure:: Images/slug_1.png
   :alt: T3AI dasebord

.. figure:: Images/slug_2.png
   :alt: T3AI dasebord

- **Step 2** - Select your AI powered Slug and click on save and your slug is generated !

.. figure:: Images/slug_3.png
   :alt: T3AI dasebord

**You can Generate slugs for Blog and News Page Also!**

