.. include:: Includes.txt

T3AI Schema
------------

AI Schema for Page
~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm38r3r2p0zwj8hspgc8cc7h3?embed_v=2" loading="lazy" title="AI schema" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Boost your site’s visibility with T3AI’s AI Schema feature! Instantly generate structured data, helping search engines understand your content better and improving your SEO performance. With just a few clicks, make your TYPO3 site more search-friendly and accessible!

- **Step 1** - Select a page from the page tree and choose 'AI Schema' from the T3AI dropdown menu. 

  - Navigate to the SEO tab and select the TYPO3 AI Schema feature.

.. figure:: Images/ai_schema.png
   :alt: T3AI dasebord

.. figure:: Images/ai_schema1.png
   :alt: T3AI dasebord

- **Step 2** - As per your need Select your AI model and pages and AI prompt for generating schema.

  - Select schema type i.e Product , website

  - Select for what to generate to schema for example news , blog

.. figure:: Images/ai_schema2.png
   :alt: T3AI dasebord

**Step 3** - Now, select the AI-generated schema and click on 'Save.' Your schema is now implemented on your selected pages.

.. figure:: Images/ai_schema3.png
   :alt: T3AI dasebord

AI Schema for Blog
~~~~~~~~~~~~~~~~~~

You can generate Schema for blogs also please follow below Steps.

 - Go to blog page/You can Select page from Drop dropdown

 - Select Record type Blog
 
.. figure:: Images/blog_schema.png
   :alt: T3AI dasebord


AI Schema for News
~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm38rpjgh10by8hsprwotjbcf?embed_v=2" loading="lazy" title="AI schema News" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

You can Generate Schema For news also,follow below steps.

Before using the Generate AI Schema feature for news, you need to configure and verify the settings in the Settings tab.

- Go to the Settings tab and select T3AI from the list.

- Then, navigate to the SEO tab, scroll to the bottom, and enter your News detail page ID to enable schema generation.

.. figure:: Images/News_detailpage.png
   :alt: T3AI dasebord

- Select Record type News and select News for which you want to generate Schema
  
.. figure:: Images/Schema_news.png
   :alt: T3AI dasebord
