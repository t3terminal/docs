.. include:: ../Includes.txt


=========================
For Administrators
=========================

Are you a TYPO3 administrator looking to boost your site's efficiency? Expand your TYPO3 skills with T3AI Resources for TYPO3 Administrators. Learn how T3AI can enhance your site management capabilities.

Settings
~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/showcase/cm1gm7tws00c214kkyqjyrdhz?demo=47 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Customize your preferences with any type of model or feature. Follow these simple steps to adjust your settings:

- Navigate to the "Extension Settings" tab.
- Verify your settings.
- Click on "NS_AI Setting."
- Choose the module you want to customize under "Global Configuration."
- Click on "Save Settings."

.. figure:: Images/Configurations.png
   :alt: AIpage

.. figure:: Images/Configurations_1.png
   :alt: AIpage

Report Issues and Request Features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you find any issues or want to add any custom feature contact us at `Contact <https://t3planet.com/contact>`_


Restrict Prompts
~~~~~~~~~~~~~~~~

As an administrator, If you want to restrict the prompts managers add/edit/delete data (like SEO prompts, Page prompts, etc) to editors. You can simply exclude all prompts management while configuring your editor's users or user groups.

Using your administrator-level access, You can easily manage prompts with this "Manage Prompts" guidance - `View Prompts <https://docs.t3planet.com/en/latest/ExtNsT3AI/ForEditors/Prompts/Index.html>`_


APIs of T3AI Extension
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the T3AI TYPO3 Extension's APIs, you can extend and customize functionalities according to your needs.

*This feature will be released in the upcoming new version*

AI System Log 
~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1japm4e3eozouou3zmbzba3 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Track all AI system activities and monitor how your backend users interact with the AI models.

- **Step 1** - Log in to your TYPO3 backend and navigate to the T3AI dashboard.

- Select the **'Techies'** tab and click on **'AI Log Manager.'**

.. figure:: Images/AIlog.png
   :alt: AI log

- **Step 2** - Open AI Log Manager

.. figure:: Images/Log.png
   :alt: AI log

- **Step 3** - Simply search for the page and select all the required fields from the dropdown menu.

     - **AI Engine** - It displays the types of AI engine logs you want to view.

     - **AI Models** - Select AI Engine Model types. For i.e GPT 4.0

     - **Module** - This Indicates T3AI module Logs. For SEO, Translation.

     - After Selecting Your Required field Click on Filter and your AI model Logs are generated.

.. figure:: Images/Log_1.png
   :alt: AI log

- **Step 4** - You can reset your AI model logs by clicking on the 'Reset' button.

System AI Log
~~~~~~~~~~~~~~

Track all AI system activities and monitor how your backend users interact with the AI models.

- **Step 1** Before Utilizing AI at system log feature you need to enable feature from your extension configuration.

- Go to Extension Configuration

- Select Ns_t3ai

- Navigate to feature toggle tab and find “ Enable System Log Solution “ options.

- Check the option and save changes

.. figure:: Images/AI_log1.png
   :alt: AI log

.. figure:: Images/Log_solution.png
   :alt: AI log

- **Step 2** - Now move to System module and navigate to log tab.

.. figure:: Images/Log_2.png
   :alt: AI log

- **Step 3** - Go to your System log and click on AI suggestion.

.. figure:: Images/Log_3.png
   :alt: AI log

- **Step 4** - Click the button to have the AI suggest solutions for your system log errors.

.. figure:: Images/Log_4.png
   :alt: AI log

Installation Wizard
~~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm2vzh6tu2539esgvxjqf1h62?embed_v=2" loading="lazy" title="TYPO3 AI Setup wizard" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Get started with T3AI quickly and easily using our Installation Wizard! Follow simple steps to set up T3AI on your TYPO3 site, so you can start enjoying AI-powered features in no time. The wizard guides you through the process, making installation smooth!

This features help you during installtion of T3AI TYPO3 AI Extension.

.. figure:: Images/Installation_wizard.png
   :alt: AI log

You can also open wizard from Module > click on Icon Setting as showing in below Screenshot

.. figure:: Images/Wizard.png
   :alt: AI log

Setting tab enhancement
~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm38ok2eq0xgs8hsp6r4xm68r?embed_v=2" loading="lazy" title="Setting tab enhancement" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


With the latest enhancement, you can now enable all features directly from the Settings tab. This streamlined access makes it easier to customize and manage your options, saving you time and offering more control over your settings in one convenient location.

- **Step 1** - Go to T3AI module , Click on T3AI Setting tab.

.. figure:: Images/Setting_tab.png
   :alt: AI log

**Step 2** - Implement Necessary Performance Changes for T3AI Features.

  - Click on save changes.

.. figure:: Images/Setting.png
   :alt: AI log


Block AI Scrapping 
~~~~~~~~~~~~~~~~~~~

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src="https://app.supademo.com/embed/cm38p1r7g0xsd8hspf30l5gmn?embed_v=2" loading="lazy" title="Block AI Scrapping" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


Protect your website’s content from unwanted AI bots with our Block AI Scraping feature. Keep your content safe and secure with just one click!

- **Step 1** - Go to site management module in your TYPO3 Backend.

 - Select Site tab from list

 - Navigate to tab and Edit your site configurations.

 - Go to static routes in Site configurations.

.. figure:: Images/Scrappng.png
   :alt: AI log

**Step 2** - Prevent bots for scraping your content with customizing required confugurqationns.
 
 - Select type of files & Route Type i.e robots .txt

 - Select type of bot to prevent from AI Content scraping.

.. figure:: Images/Scrapping.png
   :alt: AI log

Then, click on 'Generate AI Scraping File,' and your web content will now be protected from AI bots.

Auto-translate-prefix
~~~~~~~~~~~~~~~~~~~~~~~~

Tagging for automatically translated pages is now supported with a new control option, allowing users to manage and apply tags easily, improving the organization of translated content on the website.

For Administrators
~~~~~~~~~~~~~~~~~~

To enable tagging for automatically translated pages and content, the process of activating translated pages was updated to include a control option. This information is passed to the Page Context Fluid template, where it can be used to customize the page's appearance. You can also use this feature easily in the extension's Partial.

.. code-block:: Python

    <f:if condition="{data.tx_nst3ai_content_not_checked}" >
        <div style="background: #006494; border: #0000cc 1px solid; color: #fff; padding: 10px; text-align: center">
            <f:translate key="LLL:EXT:ns_t3ai/Resources/Private/Language/locallang_be:preview.flag" extensionName="ns_t3ai" />
            <f:if condition="{data.tx_nst3ai_translated_time} > 0" >
                <f:format.date format="{dateFormat}">{data.tx_nst3ai_translated_time}</f:format.date>
            </f:if>
        </div>
    </f:if>

**Backend Image**

.. figure:: Images/Backend_Image.png
   :alt: AI log


**Frontend image without preview mode**

.. figure:: Images/Frontend_without_Preview.png
   :alt: Frontend image without preview


For Editors
~~~~~~~~~~~~

To enable tagging for automatically translated pages and content, the "Page Turned On" process for translated pages was updated to include a control option.

During each translation, the fields are automatically updated. The fields "Last translation date" and "T3AI Translated Content has not been checked" are transferred to the page object and can be used in Fluid templates.

This allows you to control information and notes in the Fluid template if needed, but a TYPO3 administrator or developer must add this feature to the template first.

When an editor previews a hidden page translated by T3AI, a T3AI badge will appear alongside the "Preview" badge in the upper right corner.

**Backend Image**

.. figure:: Images/Backend_Image.png
   :alt: Backend image
 

**Preview mode image**

.. figure:: Images/Frontend_with_Preview.png
   :alt: Preview mode image


**Frontend image without preview mode**

.. figure:: Images/Frontend_without_Preview.png
   :alt: Frontend image without preview

