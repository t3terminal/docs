.. include:: ../Includes.txt


=========================
For Integrators
=========================

Are you a TYPO3 integrator looking to boost the functionality of the T3AI TYPO3 Extension? Enhance your TYPO3 integration skills with T3AI resources tailored for TYPO3 Integrators. Discover how T3AI can elevate your TYPO3 integration capabilities.

Extensions Localization (XLF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1j05tca38eiououc88mc21l  loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Make your TYPO3 extensions for a global audience with easy-to-use XLF file AI translations for easy localization.

Struggling with language issues in your TYPO3 product? T3AI is here to help. Expand your product globally without language barriers by using our easy-to-use T3AI  XLF translations. Transform your TYPO3 extensions into multilingual ones in just a few clicks!

- **Step 1:** Navigate to the T3AI module, Click on “ Translation Tab”.

- **Step 2:** Open The  “ Extensions Localization (XLF) “  Module.

.. figure:: Images/localization.png
   :alt: openstreet_map_Leaflet_Library

- **Step 3:** Select your TYPO3 extension from your list. You can also search by text or filter files directly from the module using the additional features.

.. figure:: Images/localization_1.png
   :alt: openstreet_map_Leaflet_Library


- **Step 4:** - Click on the "New Localization" button to translate your extension file into another language.

1.Navigate to "Installed Extensions" and select your extension.

2.Choose the default language file name.

3.Select the target language, e.g., German.

4.Next, choose the "Translation Mode" type. You have two options:

    **a.** **Translate the entire file and overwrite any existing translations:** This means T3AI will completely translate your extension files from scratch.

    **b.** **Translate only the missing items:** T3AI will only translate terms or lines that are missing or have not been translated yet.

Click on the **“ Translate File “** button.

.. figure:: Images/localization_2.png
   :alt: openstreet_map_Leaflet_Library

- **Step 5:** Your translated file has been created. You can now edit and save it. Your XLF file is ready!

.. figure:: Images/localization_3.png
   :alt: openstreet_map_Leaflet_Library

.. figure:: Images/localization_4.png
   :alt: openstreet_map_Leaflet_Library


Report Issues and Request Features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Report an Issue
****************

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kbofin0184e4ytqtxh42vn loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Suggest Features
*****************

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1kc47be01cge4ytxkwx6rps loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


If you find any issues or want to add any custom feature contact us at `Contact <https://t3planet.com/contact>`_

Restrict Prompts
~~~~~~~~~~~~~~~~
As an integrator, If you want to restrict the prompts managers add/edit/delete data (like SEO prompts, Page prompts, etc) to editors. You can simply exclude all prompts management while configuring your editor's users or user groups..

Using your integrator-level access, You can easily manage prompts with this “Manage Prompts” guidance  - `View Prompts <https://docs.t3planet.com/en/latest/ExtNsT3AI/HowToUse/ForEditors/Index.html#prompts>`_

One-Click Page Translations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This feature allows you to translate pages and their content elements from the default language to other languages.

.. figure:: Images/Translate_with_AI.png
   :alt: AI log

There are two options for Translatation

.. figure:: Images/Translation_option.png
   :alt: AI log

- **Translate Page**: This option allows you to translate the page into the selected or all other languages, but it only translates the pages, not the content.

- **Translate Page and & content**: This will Translate the Page along with its content from Defualt language to Selected/All languages

AI translation
~~~~~~~~~~~~~~~~~

T3AI translations have four main features for Page/ Content translation. follow below steps to Use this features

- **Step 1** - go to Page module select the page

- **Step 2** - select the page & click on edit Page property

- **step 3** - go to Tab **T3AI**

.. figure:: Images/T3AI-Translate.png
   :alt: AI log

Lets undertstand all this features in detail one by one.

Mass Pages Translation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1c3v1ru1de1ouou851dw0lr loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Update numerous translated pages simultaneously. Refresh your website content using precise, current AI-driven translations.

.. Note:: For Mass page translation, in all pages Allow Mass translation option should be enabled and if you add new pages do not forgot to perform step 5.

you can translate multiple pages and their content from the default language to various other languages efficiently,follow below steps to use this feature.

- **Step 1** - go to page module

- **Step 2** - Select the page & Click on edit page properties

- **Step 3** - go to tab T3 AI

- **Step 4** - Enable option Allow mass translation

.. figure:: Images/Mass.png
   :alt: AI log

- **Step 5** - click on drop Down T3AI> Go to Mass translation> Click on **Add this page to scheduler queries**

.. figure:: Images/Schedule_mass_translation.png
   :alt: AI log

Whenever the Scheduler will run successfully, all the pages will translate 

.. figure:: Images/Scheduler.png
   :alt: AI log

.. figure:: Images/Translation_done.png
   :alt: AI log

Run Mass Translation: From TYPO3-CLI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**T3AI Mass Translation feature** allows users to efficiently translate multiple elements or entire pages into different languages simultaneously.This is typically useful in multilingual websites where translating individual elements manually would be time-consuming.

To run a mass translation from the CLI (Command Line Interface), follow these general steps

- **Step 1:** Go to your command line/Terminal.

- **Step 2:** Run the following command

.. code-block:: Python

   Syntax: <php-path> <typo3-bin-path> scheduler:run --task=<id> -f

   Example: /usr/bin/php typo3/sysext/core/bin/typo3 scheduler:run --task=2 -f

Auto translation
~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1c1wphj1d0mouou97aj2alh loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

This feature Automatically translate pages and content from the site's default language to all other languages, ensuring consistency,follow below steps to use this feature

- **Step 1** - go to page module

- **Step 2** - Select the page & Click on edit page properties

- **Step 3** - go to tab T3AI

- **Step 4** - Enable option Allow auto translation

.. figure:: Images/Auto.png
   :alt: AI log

.. figure:: Images/AutoTranslate_1.png
   :alt: AI log

Whenever you add any content in Defualt language it will automatically translate content into other languages.

.. figure:: Images/Autotranslate_2.png
   :alt: AI log


Re-translation
~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1c353mr1dbdououj73snlc8 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

If you update content in the default language, it will re-translate it into other languages. This ensures that changes are reflected in all translations

.. figure:: Images/Retranslate_1.png
   :alt: AI log

whenever you update any content element,this feature will allow re translation in other languages as well

.. figure:: Images/Retranslate_2.png
   :alt: AI log

Recursive Mass-Translate
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/embed/cm1q9wazk1gkde4ytluhdggjd?v_email=EMAIL&embed_v=2 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

It will Translate all pages from the default language into other languages with one action, simplifying the translation process,follow below steps to use this feature

.. Note:: For recursive mass translation, ensure the mass translation option is enabled for all pages and follow step 2 when adding new pages.

- **Step 1** - Please Enable "**Allow Recursive Mass-Translate?**" from Root page property

.. figure:: Images/Recursive_tran.png
   :alt: AI log

- **Step 2** - Select root page > click on drop Down T3AI > Go to Mass translation> **Click on Recursively add this page to the scheduler queue**

.. figure:: Images/Recursive.png
   :alt: AI log

Whenever the Scheduler will run successfully, all the pages will translate 

.. figure:: Images/Scheduler.png
   :alt: AI log

.. figure:: Images/Retranslate_2.png
   :alt: AI log


AI System Log
~~~~~~~~~~~~~~~~

Track all AI system activities and monitor how your backend users interact with the AI models,follow below steps to check AI log.

- **Step 1** - Log in to your TYPO3 backend and navigate to the T3AI dashboard.

- Select the **'Technical'** tab and click on **'AI Log Manager.'**

.. figure:: Images/AIlog.png
   :alt: AI log

- **Step 2** - Open AI Log Manager

.. figure:: Images/Log.png
   :alt: AI log

- **Step 3** - Simply search for the page and select all the required fields from the dropdown menu.

     - **AI Engine** - It displays the types of AI engine logs you want to view.

     - **AI Models** - Select AI Engine Model types. For i.e GPT 4.0

     - **Module** - This Indicates T3AI module Logs. For SEO, Translation.

     - **Scope** - This indicates T3Ai scops like meta title,meta description etc

     - After Selecting Your Required field Click on Filter and your AI model Logs are generated.

.. figure:: Images/Log_1.png
   :alt: AI log

- **Step 4** - You can reset your AI model logs by clicking on the 'Reset' button.


AI Meet EXT:content_blocks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Next-generation AI. We plan to create custom AI-driven elements using the popular EXT:content_blocks. Stay tuned for updates!

*This feature will be released in the upcoming new version*

AI Meet Crowdin
~~~~~~~~~~~~~~~~

Contribute your translations of TYPO3 core and extensions through the TYPO3 community's Crowdin platform.

*This feature will be released in the upcoming new version*
