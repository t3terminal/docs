
.. include:: ../Includes.txt

===============
Video Tutorials
===============

User Guide in Videos
====================
Experience the power of TYPO3 AI with ease! T3AI's GUIDIE feature provides step-by-step instructions, making your TYPO3 AI journey smoother and faster than ever before.

.. raw:: html

 <div style="position: relative; box-sizing: content-box; max-height: 80vh; max-height: 80svh; width: 100%; aspect-ratio: 1.7918032786885245; padding: 40px 0 40px 0;"><iframe src=https://app.supademo.com/showcase/cm1gm7tws00c214kkyqjyrdhz?demo=1 loading="lazy" title="AI Co pilot" allow="clipboard-write" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>