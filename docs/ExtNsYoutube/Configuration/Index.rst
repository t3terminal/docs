.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

1. Add Youtube API key
======================

	.. rst-class:: ol-bignums

	#. Switch to the root page of your site.

	#. Switch to the **Template module** and select *Constant Editor*.

	#. Select Category = PLUGIN.TX_NSYOUTUBE (5)

	#. Generate API key from this URL: `https://developers.google.com/youtube/v3/getting-started <https://developers.google.com/youtube/v3/getting-started>`_

.. figure:: Images/configure_api.jpeg
	:alt: TYPO3_NS_YOUTUBE_Config_1
	

2. Add Plugin To Page
======================

	Add this great plugin to the page where you want to show your youtube videos and configure this plugin as per your requirement.

.. figure:: Images/configure_plugin_embed_playlist.jpeg
	:alt: TYPO3_NS_YOUTUBE_Config_4
	
.. figure:: Images/configure_plugin_embed_channel1.jpeg
	:alt: TYPO3_NS_YOUTUBE_Config_5
		
.. figure:: Images/configure_plugin_embed_channel2.jpeg
	:alt: TYPO3_NS_YOUTUBE_Config_5
	
.. figure:: Images/configure_plugin_single_video.jpeg
	:alt: TYPO3_NS_YOUTUBE_Config_5

.. figure:: Images/GDPR_OPTION_1.jpeg
	:alt: TYPO3_NS_YOUTUBE_GDRP_1
	
.. figure:: Images/GDPR_OPTION_2.jpeg
	:alt: TYPO3_NS_YOUTUBE_GDPR_2

- **GDPR Box** -> Make sure your website collects all required user consent. Youtube allows you to display consent notices.
	
.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches'
from the top panel. The 'Clear cache' function of the install tool will also
work perfectly.