
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_PWA
===========

What does it do?
================

The TYPO3 PWA (Progressive Web App) extension is a powerful tool designed to transform your TYPO3 website into a progressive web application. This extension leverages modern web technologies to offer a mobile app-like experience directly from the web browser, without the need for users to download an app from any app store.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/typo3-pwa-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-ns-pwa
	- Front End Demo: https://demo.t3planet.com/t3-extensions/pwa
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support