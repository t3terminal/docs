
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Quick & Easy configuration of "All Lightbox" into TYPO3
=======================================================

.. rst-class:: ol-bignums

#. Switch to the *Template module* and select *Constant Editor*

#. Select Category *PLUGIN.TX_NSALLLIGHTBOX* You can configure it as per your requirement.

   .. figure:: Images/select_lightbox_updated.jpeg
      :alt: Configuration 1

#. Behavior: *Enlarge on Click [Checkbox]*.
   
   .. rst-class:: bignums

      #. You can only setup "Text & Media" TYPO3 Elements. (Because, We have setup everything with fluid_styled_content)
      
      #. Go to Images tab & Click on "Enlarge on click" checkbox. Checkout below screenshot.

   .. figure:: Images/TYPO3_All_In_One_Lightbx_Modalbox_Extension_NITSAN_Backend_Select_Enlarge_on_Click-min.jpeg
      :alt: Configuration 2



.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches'
from the top panel. The 'Clear cache' function of the install tool will also
work perfectly.
