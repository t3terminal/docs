.. include:: ../Includes.txt


News Comment Backend Module
****************************

If "Set Approval by admin" is checked in Constants then Comments added by visitors will not be displayed automatically on News Page. Admin Can Approve comments from BE module.
Admin can approve comments by following ways:

**Step 1** Select News Module from Sidebar

**Step 2** Select "News" Folder where News added

You'll see two tabs  Dashboard and Comment List, Let's check it one by one how it works.

============
Dashboard
============

In Dashboard you'll see total number of comments,total approved comments and total disapproved comments.

.. figure:: Images/News_Comment_Dashboard.png
   :alt: Approve Comment from backend

==============
Comments List
==============

In this you can admin can approve/disapprove and can delete comments.

.. figure:: Images/Comments_List.png
   :alt: Approve Comment from E-mail

That’s it, Now you can enjoy comments of your website visitors :)