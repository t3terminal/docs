
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_guestbook
================

.. figure:: Images/ext-banner.jpg
   :alt: Extension Banner 

NS Guestbook extension is an open source TYPO3 guest book plugin with spam protection and many customization options. NS Guestbook gives functionality for your guests to share their reviews, ratings, contact details or personal details with experience sharing and much more.

Guest user adds their details and can see all guest user’s guest book detail. Admin can see stories which are submitted by users in Guest book tab. Admin can edit, delete and published/unpublished guest entry. The feature of guest entry auto publish is configurable from the backend.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/guestbook-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-guestbook
	- Front End Demo: https://demo.t3planet.com/t3-extensions/guestbook	
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support