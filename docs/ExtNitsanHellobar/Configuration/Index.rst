.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Configure your site at Hellobar.com
===================================

Go to Hellobar.com and Login to your account. Create a Site there and configure the Hellobar.

.. figure:: Images/configure_hellobar.png
   :alt: Configure hellobar

Once completed, get installation code for your site from hellobar.com

.. figure:: Images/copy_code.jpeg
   :alt: Copy Hellobar code

Configure Hellobar at backend
=============================

Place the installation code at "Your Hellobar Code" field at Constants. Also set where to add this hellobar code in you page source. you can either set at Header or Footer.

.. figure:: Images/configure_hellobar_at_backend.jpeg
   :alt: Configure hellobar at backend