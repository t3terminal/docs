
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Activate Revolution Licenses
============================

**Step 1:** Go to NITSAN > Slider Revolution Module at TYPO3 Backend.

**Step 2:** Add your License key at "Slider Revolution Responsive jQuery"

**Step 3:** Add your License key at "Revolution jQuery Visual Editor"

.. figure:: Images/TYPO3-Revolution-Slider-License-Activate.png
   :alt: TYPO3-Revolution-Slider-License-Activate
   :width: 1000px

.. attention:: Use the same license key (from T3Planet) which you have used to Activate and Install the TYPO3 extension.


Backend Module: Manage Slider Revolution
========================================

.. rst-class:: ol-bignums

#. Switch to the NITSAN > Slider Revolution Backend Module.

#. Create unlimited number of Slider Revolutions with all awesome features.

.. caution:: If are getting any errors/issues with "Slider Revolution visual-editor" backend module, then you need to make sure PHP and other compatibilities with revolution jQuery visual editor.

.. figure:: Images/TYPO3-Revolution-Slider-Backend-Usage.png
   :alt: TYPO3-Revolution-Slider-Backend-Usage
   :width: 1000px


Frontend Plugin: Insert Slider Revolution
=========================================

.. rst-class:: ol-bignums

#. Go to Page > Choose your page where you want to insert Slider Revolution

#. Insert "NS Slider Revolution" Plugin

#. Choose your favourite slider (from your created at Slider Revolution backend module).

.. figure:: Images/TYPO3-Revolution-Slider-Frontend-Plugin.png
   :alt: TYPO3-Revolution-Slider-Frontend-Plugin
   :width: 1000px


.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches' from the top panel. The 'Clear cache' function of the install tool will also work perfectly.


Global Settings
===============

.. figure:: Images/01-T3Revolution.png
   :alt: 01-T3Revolution
   :width: 1000px

.. figure:: Images/02-T3Revolution.png
   :alt: 02-T3Revolution
   :width: 1000px

.. figure:: Images/03-T3Revolution.png
   :alt: 03-T3Revolution
   :width: 1000px

Slider Settings
===============

.. figure:: Images/04-T3Revolution.png
   :alt: 04-T3Revolution
   :width: 1000px

Drag-n-Drop Slider
==================

.. figure:: Images/05-T3Revolution.png
   :alt: 05-T3Revolution
   :width: 1000px

Manage Sliders
==============

.. figure:: Images/06-T3Revolution.png
   :alt: 06-T3Revolution
   :width: 1000px