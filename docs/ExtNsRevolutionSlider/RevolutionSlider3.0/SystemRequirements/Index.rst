.. include:: ../Includes.txt

.. _system:

===================
System Requirements
===================

   .. rst-class:: bignums

   #. Follow checklist from official slider revolution at https://www.sliderrevolution.com/documentation/system-requirements/

   #. Memory Limit (256M)

   #. Upload Max. Filesize (256M)

   #. Max. Post Size (256M)