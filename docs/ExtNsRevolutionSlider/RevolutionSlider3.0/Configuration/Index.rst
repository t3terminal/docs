
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Activate Revolution Licenses
============================

**Step 1:** Go to NITSAN > Slider Revolution Module at TYPO3 Backend.

**Step 2:** Click on "Activation" and registered our provided license key.

.. figure:: Images/TYPO3-Revolution-Slider-License-Registered.png
   :alt: TYPO3-Revolution-Slider-License-Registered
   :width: 1000px

.. attention:: Use the same license key (from T3Planet) which you have used to Activate and Install the TYPO3 extension.


Include TypoScript
==================

.. rst-class:: ol-bignums

#. Switch to Web > Template module

#. Click on "Edit the whole template record" > Includes tab

#. Add "[NITSAN] Slider Revolution (ns_revolution_slider)"

.. figure:: Images/TYPO3-Revolution-Slider-Include-TypoScript.png
   :alt: TYPO3-Revolution-Slider-Include-TypoScript
   :width: 1000px


Enable/Disable jQuery
=====================

.. rst-class:: ol-bignums

You can enable or disable jQuery form "Constant Editor". By default, the extension enable jQuery to include at frontend. If your website already contains jQuery then you can disable it for better speed and performance.


Backend Module: Manage Slider Revolution
========================================

.. rst-class:: ol-bignums

#. Switch to the NITSAN > Slider Revolution Backend Module.

#. If you are beginner to Slider Revolution, then we highly recommend to learn it from official documentation at https://www.sliderrevolution.com/help-center/

.. figure:: Images/TYPO3-Revolution-Slider-Backend.png
   :alt: TYPO3-Revolution-Slider-Backend
   :width: 1000px


Frontend Plugin: Insert Slider Revolution
=========================================

.. rst-class:: ol-bignums

#. Go to Page > Choose your page where you want to insert Slider Revolution

#. Insert "[NITSAN] Slider Revolution" Plugin

#. Choose your favourite slider (from your created at Slider Revolution backend module).

.. figure:: Images/TYPO3-Revolution-Slider-Plugin.png
   :alt: TYPO3-Revolution-Slider-Plugin.png
   :width: 1000px

.. figure:: Images/TYPO3-Revolution-Slider-Frontend-Plugin.png
   :alt: TYPO3-Revolution-Slider-Frontend-Plugin
   :width: 1000px


.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches' from the top panel. The 'Clear cache' function of the install tool will also work perfectly.