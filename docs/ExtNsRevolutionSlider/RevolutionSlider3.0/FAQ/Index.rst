
.. include:: ../Includes.txt

.. _faq:

===
FAQ
===

How to set WordPress' media path to TYPO3's fileadmin?
======================================================

Currently, all the slider revolution plugin's assets (images, video) are stored at WordPress's default media path. If you want to keep all the media assets to TYPO3 core's fileadmin folder, you will need to create a symlink path using CLI commands. You will need to run the below commands at Installation and Update the new version.

.. code-block:: python

   - mv typo3conf/ext/ns_revolution_slider/vendor/wp/wp-content/uploads public/fileadmin/revslider
   - chmod -R 755 public/fileadmin/revslider/uploads
   - rm -rf typo3conf/ext/ns_revolution_slider/vendor/wp/wp-content/uploads
   - ln -sf public/fileadmin/revslider/uploads typo3conf/ext/ns_revolution_slider/vendor/wp/wp-content/uploads