
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Setup Slider Revolution
=======================

.. rst-class:: ol-bignums

#. Switch to the *Template module* and select *Constant Editor*

#. Global Configuration: Select Category as follows *PLUGIN.TX_NSREVOLUTIONSLIDER_SLIDER (13)* You can configure it as per your requirement.

   .. figure:: Images/TYPO3-Revolution-Slider-Global-Configuration.png
      :alt: Configuration 1
      :width: 1000px

#. *Include Jquery* if you have not included yet in your project.

   .. figure:: Images/TYPO3-Revolution-Slider-Include-jQuery.png
      :alt: Configuration 3
      :width: 1000px

#. Create *Storage Folder* and Add "Slider" and "Slider Item" Records.

   .. figure:: Images/TYPO3-Revolution-Slider-Gallery-Records.png
      :alt: Configuration 4
      :width: 1000px

#. Now, Just Insert "NS Slider Revolution" Plugin, Choose your Storage Folder & Configure Slider Options.

   .. figure:: Images/TYPO3-Revolution-Slider-Add-Plugin.png
      :alt: Configuration 5
      :width: 1000px


.. _Clearing-the-cache:

Clearing the cache
==================

Please use the buttons 'Flush frontend caches' and 'Flush general caches'
from the top panel. The 'Clear cache' function of the install tool will also
work perfectly.
