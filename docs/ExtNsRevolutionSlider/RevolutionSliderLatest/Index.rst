.. include:: Includes.txt

================================
Slider Revolution Latest (>= v12)
================================

.. toctree::
   :glob:

   SystemRequirements/Index
   MigrationFrom3toLatest/Index
   Installation/Index
   Configuration/Index