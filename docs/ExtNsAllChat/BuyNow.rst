.. include:: Includes.txt

==============
Get this Extension
==============

Get Latest Version of this extension from https://t3planet.com/typo3-chat-extension or https://extensions.typo3.org/extension/ns_all_chat/
