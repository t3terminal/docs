
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_instagram
================

.. figure:: Images/TYPO3-instagram-banner.jpg
   :alt: Extension Banner 

What does it do?
================

Plugin to display Instagram Feed with clean, customizable and responsive feeds from your Instagram account.

It’s awesome TYPO3 extension which provides you a very attractive frontend display of Instagram posts at your TYPO3 website.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/instagram-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-instagram
	- Front End Demo: https://t3-extension.t3planet.com/pro/typo3-instagram	
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support