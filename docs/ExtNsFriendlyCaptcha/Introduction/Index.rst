
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_friendlycaptcha
===============

.. figure:: Images/ext_banner.jpg
   :alt: Extension banner 


What does it do?
================

EXT:ns_friendlycaptcha is a TYPO3 extension that generate unique crypto puzzle for each visitor. As soon as the user starts filling a form it starts getting solved automatically. Solving it will usually take a few seconds. By the time the user is ready to submit, the puzzle is probably already solved. it prevent spammers and hackers from inserting dangerous or frivolous code into online forms.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/friendly-captcha-typo3-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-friendly-captcha
	- Front End Demo: https://demo.t3planet.com/t3t-extensions/friendly-captcha
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support