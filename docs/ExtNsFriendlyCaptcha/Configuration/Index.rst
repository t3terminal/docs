.. include:: ../Includes.txt

=============
Configuration
=============

You can configure all the settings of ns_friendlycaptcha as described below:

- **Step 1:** Go to Template.

- **Step 2:** Select root page.

- **Step 3:** Select Constant Editor > Plugin.NSFRIENDLYCAPTCHA (13).

- **Step 4:** Add Friendly captcha Site key.

- **Step 5:** Add Friendly captcha secret key,the secret key authorizes communication between your application backend and the friendlycaptcha server to verify the user's response. The secret key needs to be kept safe for security purposes..

.. note::

   For generating the Site and Secret key, please refer to the following link:
   `FriendlyCaptcha Documentation <https://docs.friendlycaptcha.com/#/installation?id=_1-generating-a-sitekey>`_

- **Step 6:** Now, you can configure all the options which you want eg., Auto Check,Check on focus and Manual, See below screenshot.

- **Step 7:** While enabling the EU endpoint it gurantee that the personal Information (Like IP address) never leave the EU, learn more from here https://docs.friendlycaptcha.com/#/eu_endpoint

.. figure:: Images/Friendlycaptcha_Configuration_1.jpeg
   :alt:  Configuration1
   
.. figure:: Images/Friendlycaptcha_Configuration_2.jpeg
   :alt:  Configuration2