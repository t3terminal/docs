.. include:: ../Includes.txt

========================
Extension Configurations
========================

You can insert the timeline extension according to your website need. there are multiple layouts like Standard, news & events with multiple styles available for your websites requisite.
Multiple animations & effects gives amazing look to your site.


**Step 1.** Insert extension from the page content > TimeLine

**Step 2.** Choose Type > Standard | News | Events

**Step 3.** Select Style Variation > Style 01 | Style 02 | Style 03...Style 18.	

**Step 3.** Do Required configurations according to your website's need.

.. figure:: Images/ns-timeline-typo3-extension1.jpeg
   :alt: ns-timeline-typo3-extension1

.. figure:: Images/ns-timeline-typo3-extension2.jpeg
   :alt: ns-timeline-typo3-extension2

.. figure:: Images/ns-timeline-typo3-extension3.jpeg
   :alt: ns-timeline-typo3-extension3